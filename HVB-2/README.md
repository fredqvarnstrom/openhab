# HVB-2 sub-project of WiF560


## Install kivy on RPi.
       
    sudo apt-get install libpython-dev   
    sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-{bad,base,good,ugly} 
    sudo pip install Cython==0.23
    sudo pip install git+https://github.com/kivy/kivy.git@master
      
## Enable auto-start of GUI app

We do not need to run `helper.py` as described in *master* branch.
    
    sudo nano /etc/profile

And add this:
    
    /usr/bin/sudo /usr/bin/python /home/pi/HVB-2/app/main.py
    
## Disable screen saver on RPi
    
    sudo apt-get install x11-xserver-utils

    sudo nano /etc/X11/xinit/xinitrc
    
Add following at the end of the file.
    
    xset s off         # don't activate screensaver
    xset -dpms         # disable DPMS (Energy Star) features.
    xset s noblank     # don't blank the video device

Change another file.
    
    sudo nano /etc/lightdm/lightdm.conf

In the SeatDefaults section it gives the command for starting the X server which I modified to get it to turn off the screen saver as well as dpms.
    
    [SeatDefaults]
    xserver-command=X -s 0 -dpms
    
## Backlight adjustment
    
    sudo nano /etc/udev/rules.d/backlight-permissions.rules

Insert the line:

    SUBSYSTEM=="backlight",RUN+="/bin/chmod 666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power"

## Enable auto-starting of openHAB web app.

We will use `pagekite` service to host web app.
    
    cd ~
    wget https://pagekite.net/pk/pagekite.py

At first, we need to add web service
    
    sudo python pagekite.py 8080 foo.pagekite.me
    
After following the setup, we could see our openHAB app at `https://foo.pagekite.me`

Stop running of python script with `CTRL+C`, and add this to `/etc/rc.local`
    
    sudo nano /etc/rc.local
    
And add this before `exit 0`
    
    (/usr/bin/python /home/pi/pagekite.py 8080 foo.pagekite.me)&

And reboot!

## Install libraries for Window device

    https://github.com/PaulStoffregen/OneWire
    
    