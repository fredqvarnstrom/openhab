/*
	
	Churchie  Window1
	
	This device will have a windows and outer temperature sensor.
	
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>

const char* device_name = "window1";   // Change this value for each WiFi devices

int upload_interval = 30;       // Uploading interval of DS18B20's data in seconds.

/*    --------  Constant values -----------        */

//const char* ssid = "WiFi560-AP";              // RPi3's AP name
//const char* password = "Shane_wifi560";       // RPi3's AP password
//const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself


const char* temp_suffix = "/temperature";

const char* w_open_set_suffix = "/w_open/set";
const char* w_open_state_suffix = "/w_open/state";

const char* w_close_set_suffix = "/w_close/set";
const char* w_close_state_suffix = "/w_close/state";

// Include and Configure DS18B20 SENSOR
#define ONE_WIRE_BUS  13       // D7, GPIO13
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);

int PIN_W_OPEN = 5;     // Use D1, GPIO5
int PIN_W_CLOSE = 4;    // Use D2, GPIO4
int LED_PIN = 16;		// Built-in LED pin

// Global variables
// DHT dht(DHTPIN, DHTTYPE);

float humidity, temp_c, temp_f, heatindex;

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;

const char* w_open_state = "OFF";
const char* w_close_state = "OFF";

char buf_pub_topic[50];
char buf_sub_topic[50];

int counter = 0;
float acc_temp = 0;

void setup() {
  pinMode(PIN_W_OPEN, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(PIN_W_OPEN, LOW);
  pinMode(PIN_W_CLOSE, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(PIN_W_CLOSE, LOW);
  
  pinMode(LED_PIN, LOW);
  digitalWrite(LED_PIN, LOW);
  
  Serial.begin(9600);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  
  counter = 0;
  acc_temp = 0;
}

void setup_wifi() {
  
  int attempt = 0;
  
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
	// Try to connect for 15 sec, and restart
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
	  
	digitalWrite(LED_PIN, LOW);
    delay(250);
	  digitalWrite(LED_PIN, HIGH);
    delay(250);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");
  
  // Check topic of windows open relay
  set_sub_topic(w_open_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    if (!strncmp((const char*)payload, "ON", 2)) {
        w_open_state = "ON";
        Serial.println("Turning RELAY of WINDOWS OPEN ON...");
        digitalWrite(PIN_W_OPEN, HIGH);     
    } else if (!strncmp((const char*)payload, "OFF", 3)){
      w_open_state = "OFF";
      Serial.println("Turning RELAY of WINDOWS OPEN OFF...");
      digitalWrite(PIN_W_OPEN, LOW);  
    }
  }   
  
  // Check topic of windows close relay
  else{
    set_sub_topic(w_close_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
      if (!strncmp((const char*)payload, "ON", 2)) {
            w_close_state = "ON";
            Serial.println("Turning RELAY of WINDOWS CLOSE ON...");
            digitalWrite(PIN_W_CLOSE, HIGH);   
      } else if (!strncmp((const char*)payload, "OFF", 3)){
        w_close_state = "OFF";
        Serial.println("Turning RELAY of WINDOWS CLOSE OFF...");
        digitalWrite(PIN_W_CLOSE, LOW);  
      }
    }
  }
}

void reconnect() {
  
  int attempt = 0;
  
  // Loop until we're reconnected
  while (!client.connected()) {
    if (attempt < 3)
      attempt ++;
    else
      ESP.restart();
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client1")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(w_open_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(w_close_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
	
  // Upload temperature every 30 seconds
  int attempt_temp = 0;
  do {
    DS18B20.requestTemperatures();
    temp_c = DS18B20.getTempCByIndex(0);
    // Try to read for 5 times
    if (attempt_temp < 5)
        attempt_temp ++;
    else
        break;
  } while (temp_c == 85.0 || temp_c == (-127.0));

  if (temp_c != 85.0 && temp_c != (-127.0)){
    if (counter < upload_interval - 1){
	  acc_temp += temp_c;
	  counter ++;
    }
    else{
		acc_temp += temp_c;
		char* buf_temp = new char[10];
		dtostrf(acc_temp/float(upload_interval), 5, 2, buf_temp);

		Serial.print("Temperature: ");
		Serial.println(acc_temp/float(upload_interval));
		// publish temperature value
		set_pub_topic(temp_suffix);
		client.publish(buf_pub_topic, buf_temp);

		acc_temp = 0;
		counter = 0;
	}
  }

  // publish state of window open relay
  set_pub_topic(w_open_state_suffix);
  client.publish(buf_pub_topic, w_open_state);

  // publish state of window close relay
  set_pub_topic(w_close_state_suffix);
  client.publish(buf_pub_topic, w_close_state);
	
  digitalWrite(LED_PIN, HIGH);
  
  delay(1000);   
  
}

//void ReadDHT() {
//  // Read temperature as Celsius
//  temp_c = dht.readTemperature();
//
//  // Check if any reads failed and exit early (to try again).
//  if (isnan(temp_c)) {
//    Serial.println("Failed to read from DHT sensor :-(");
//    return;
//  }
//
//  Serial.print("Temp:  ");
//  Serial.println(temp_c);
//  
//}

void set_pub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}
