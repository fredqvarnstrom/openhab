# -*- coding: utf-8 -*-
import calendar
import datetime
import glob
import os
import platform
import threading
import time
import xml.etree.ElementTree
from functools import partial
from math import cos, sin, pi

import paho.mqtt.client as mqtt
import xmltodict
from kivy.app import App
from kivy.clock import Clock
from kivy.graphics import Color, Line
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, ListProperty, ObjectProperty, NumericProperty
from kivy.uix.dropdown import DropDown
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition

import net_util
from screens.schedule.screen import ScheduleScreen

client = mqtt.Client()
cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
counter = 60 * 5        # Keep brightness for 5 min


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class WindowPopup(Popup):

    def __init__(self, **kwargs):
        super(WindowPopup, self).__init__(**kwargs)
        for i in range(4):
            self.ids['txt_window{}'.format(i)].text = App.get_running_app().get_param_from_xml('WINDOW{}'.format(i))

    def go_schedule(self, win_num):
        self.dismiss()
        App.get_running_app().cur_window = win_num
        App.get_running_app().go_screen('schedule', 'left')


class MainWidget(FloatLayout):

    def on_touch_down(self, touch):
        global counter
        _app = App.get_running_app()
        if counter == 0:
            counter = int(_app.get_param_from_xml('SCREEN_ACTIVE')) * 60
            Clock.schedule_once(_app.count_down)
            return
        counter = int(_app.get_param_from_xml('SCREEN_ACTIVE')) * 60
        super(MainWidget, self).on_touch_down(touch)
        if _app.current_title == 'menu':
            if _app.screens['menu'].ids.img_client_logo.collide_point(*touch.pos):
                _app.go_screen('pin', 'left')


class MainApp(App):
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty([])
    screens = {}                            # Dict of all screens
    hierarchy = ListProperty([])

    # Popup instances
    caution_popup = ObjectProperty(None)
    confirm_popup = ObjectProperty(None)
    confirm_param = ListProperty([None] * 10)

    conf_file_name = cur_dir + 'config.xml'
    wind_trip = NumericProperty(10)
    wind_delay = NumericProperty(5)
    rain_delay = NumericProperty(4)

    b_wind_trip = BooleanProperty(False)
    b_rain = BooleanProperty(False)
    b_close_all = BooleanProperty(False)
    status = ObjectProperty(None)

    last_cmd = ObjectProperty(None)

    cnd = NumericProperty(0)

    cur_window = NumericProperty(1)
    dropdown = DropDown()  # DropDown instance in 'WiFi Setting' screen

    en_rain = BooleanProperty(False)
    en_wind = BooleanProperty(False)
    en_temp = BooleanProperty(False)

    window_name = ListProperty([''] * 4)

    def __init__(self, **kwargs):
        global counter
        counter = int(self.get_param_from_xml('SCREEN_ACTIVE')) * 60

        self.update_window_names()
        super(MainApp, self).__init__(**kwargs)
        self.cnt = 0

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self.load_screen()
        self.caution_popup = CautionPopup()
        self.confirm_popup = ConfirmPopup()
        self.status = dict()
        for device in ['window{}'.format(i) for i in range(1, 5)]:
            self.status[device] = {
                'w_open': {'cnt': 0, 'state': False},
                'w_close': {'cnt': 0, 'state': False},
            }
        self.status['rain'] = {'state': False, 'time': -1}
        self.status['wind'] = {'state': False, 'time': -1}

        self.last_cmd = {
            'window1': 'w_close',
            'window2': 'w_close',
            'window3': 'w_close',
            'window4': 'w_close'
        }

        # self.screens['menu'].ids['sw_close_all'].bind(self.on_close_all)

        self.wind_trip = int(self.get_param_from_xml('WIND_SPEED_TRIP'))
        self.wind_delay = int(self.get_param_from_xml('WIND_DELAY'))
        self.rain_delay = int(self.get_param_from_xml('RAIN_DELAY'))
        self.update_sensor_settings()

        self.go_screen('menu', 'right')
        # self.go_screen('settings', 'right')

        Clock.schedule_interval(self.execute_relay, 1)
        Clock.schedule_interval(self.count_down, 1)
        Clock.schedule_interval(self.check_schedule, 60)
        Clock.schedule_interval(self.show_time, 1)

        client.on_connect = self.on_connect
        client.on_message = self.on_message
        # For debug
        if platform.system() == 'Windows':
            client.connect("192.168.1.103", 1883, 60)
        else:
            client.connect("localhost", 1883, 60)

        # Clock.schedule_interval(self.draw_wind_dir, 1)
        threading.Thread(target=client.loop_forever).start()

    def on_connect(self, client, userdata, flags, rc):
        """
        The callback for when the client receives a CONNECT response from the server.
        :return:
        """
        print_with_time("Connected with result code " + str(rc))

        for i in range(1, 5):
            client.subscribe('window{}/w_open/set'.format(i))
            client.subscribe("window{}/w_open/state".format(i))
            client.subscribe("window{}/w_close/set".format(i))
            client.subscribe("window{}/w_close/state".format(i))
        # TODO: add more topics to be subscribed when new window devices are added.
        client.subscribe('window1/temperature')
        client.subscribe('window1/rain')
        client.subscribe('window1/wind_speed')
        client.subscribe('window1/wind_dir')

    def on_message(self, client, userdata, msg):
        """
        The callback for when a PUBLISH message is received from the server.
        :param client:
        :param userdata:
        :param msg:
        :return:
        """
        topic = msg.topic
        message = msg.payload
        if message == 'nan':
            return False
        # print 'Topic: {}, message: {}'.format(topic, message)
        scr = self.screens['menu']

        # temperature topics
        if topic == 'window1/temperature' and self.en_temp:
            scr.ids['lb_temp_in'].text = '{} °C'.format(message)
        elif topic == 'window2/temperature' and self.en_temp:
            scr.ids['lb_temp_out'].text = '{} °C'.format(message)

        # rain topic
        elif topic == 'window1/rain' and self.en_rain:
            if message == 'ON':
                scr.ids['lb_rain'].text = '[b][color=408000]RAIN - YES[/color][/b]'
                self.on_rain_event(True)
            else:
                scr.ids['lb_rain'].text = '[b][color=408000]RAIN - NO[/color][/b]'
                self.on_rain_event(False)

        # wind topics
        elif topic == 'window1/wind_speed' and self.en_wind:
            self.on_wind_speed_event(float(message))
        elif topic == 'window1/wind_dir' and self.en_wind:
            self.draw_wind_dir(float(message))

        # status topics
        elif '/state' in topic:
            # sample topic: "window1/w_open/state"
            [device, window, state] = topic.split('/')
            self.status[device][window]['state'] = True if message == 'ON' else False

        elif '/set' in topic:
            # sample topic: "window1/w_open/set"
            self.on_event_from_openhab(topic, message)

    def on_rain_event(self, event):
        if event:
            # When rain starts
            if not self.status['rain']['state']:
                print_with_time("Attention! It's raining now!")
                self.close_all_windows()
        else:
            # When rain is just stopped
            if self.status['rain']['state']:
                self.status['rain']['time'] = time.time()
                print_with_time('Rain is stopped')
            else:
                last_time = self.status['rain']['time']
                if last_time != -1:
                    delay = int(self.get_param_from_xml('RAIN_DELAY')) * 60
                    if delay == 0:
                        return False
                    if time.time() - last_time > delay:
                        print_with_time('Rain delay starts...')
                        self.status['rain']['time'] = -1
                        self.restore_all_windows()

        # Update rain status
        self.status['rain']['state'] = event

    def on_wind_speed_event(self, speed):
        # Update GUI widgets
        self.screens['menu'].ids['lb_wind_speed'].text = '{} KPH'.format(round(speed, 2))
        self.screens['wind'].ids['lb_wind_speed'].text = 'WIND SPEED - {} KPH'.format(round(speed, 2))

        # check with threshold
        event = speed > float(self.get_param_from_xml('WIND_SPEED_TRIP'))
        if event:
            # When wind starts
            if not self.status['wind']['state']:
                print_with_time('Attention! Strong wind!')
                self.close_all_windows()
        else:
            # When wind is just stopped
            if self.status['wind']['state']:
                self.status['wind']['time'] = time.time()
                print_with_time('Safe wind...')
            else:
                last_time = self.status['wind']['time']
                if last_time != -1:
                    if time.time() - last_time > int(self.get_param_from_xml('WIND_DELAY')) * 60:
                        print_with_time("Wind delay timer starts...")
                        self.status['wind']['time'] = -1
                        self.restore_all_windows()

        # Update rain status
        self.status['wind']['state'] = event

    def restore_all_windows(self):
        """
        Restore previous status of each window
        :return:
        """
        for device in ['window{}'.format(i) for i in range(1, 5)]:
            if self.last_cmd[device] == 'w_open':
                another_relay = 'w_close'
            else:
                another_relay = 'w_open'
            self.status[device][another_relay]['cnt'] = 0
            time.sleep(1)
            self.status[device][self.last_cmd[device]]['cnt'] = 1

    def close_all_windows(self):
        """
        Close all windows
        :return:
        """
        # Stop all window relays
        for device in ['window{}'.format(i) for i in range(1, 5)]:
            for window in self.status[device].keys():
                self.status[device][window]['cnt'] = 0
                self.on_btn(device, 'stop')
        # Start window_close relays
        for device in ['window{}'.format(i) for i in range(1, 5)]:
            self.on_btn(device, 'close')

    def open_all_windows(self):
        # Stop all window relays
        for device in ['window{}'.format(i) for i in range(1, 5)]:
            for window in self.status[device].keys():
                self.status[device][window]['cnt'] = 0
                self.on_btn(device, 'stop')
        # Start window_open relays
        for device in ['window{}'.format(i) for i in range(1, 5)]:
            self.on_btn(device, 'open')

    def on_event_from_openhab(self, topic, command):
        """
        This function is called when a window(open/close) is turned on from the openhab web app.
        :param command: "ON" or "OFF"
        :param topic: "window1/w_open/set"
        :return:
        """
        [device, window, cmd] = topic.split('/')
        another_window = 'w_open' if window == 'w_close' else 'w_close'
        if command == 'ON':
            if self.b_close_all:
                print_with_time('Received ON command from openHAB, but CLOSE ALL switch is activated now, aborting...')
                return False
            elif self.status[device][window]['cnt'] > 0:
                # print_with_time('Received command from openhab: {}, but this is already opened...'.format(topic))
                return False
            elif self.status[device][another_window]['cnt'] > 0:
                # print_with_time('Received command from openhab: {}, but another is already opened...'.format(topic))
                return False
            else:
                print_with_time('Received command from openhab, executing...')
                self.check_another_window(device, window)

        else:
            self.status[device][window]['cnt'] = 0

    def start_relay_on(self, device, window):
        """
        Start process of turning relay on...
        :param device:
        :param window:
        :return:
        """
        # start relay
        self.status[device][window]['cnt'] = 1
        if window == 'w_open':
            txt = 'Opening {}... '.format(device)
        else:
            txt = 'Closing {}...'.format(device)
        print_with_time(txt)

        # Save last relay command to restore when it is safe.
        self.last_cmd[device] = window

        # popup = Popup(title='HVB-1', title_size=22, size_hint=(.6, .4),
        #               content=Label(text=txt, font_size=20))
        # popup.open()
        # Clock.schedule_once(partial(self.remove_popup, popup), 5)

    def execute_relay(self, *args):
        """
        Executing routine of all windows
        This function is called every second
        :param args:
        :return:
        """
        for device in ['window{}'.format(i) for i in range(1, 5)]:
            for window in self.status[device].keys():
                cnt = self.status[device][window]['cnt']
                if cnt == 0:
                    if self.status[device][window]['state']:
                        client.publish(topic=device + '/' + window + '/set', payload='OFF')
                    self.screens['menu'].ids['btn_' + device + '_' + window].background_color = [1, 1, 1, 1]
                elif 0 < cnt < 30:
                    print_with_time('Relay is on, device: {}, window: {}, counter: {}'.format(device, window, cnt))
                    self.status[device][window]['cnt'] += 1
                    if not self.status[device][window]['state']:
                        client.publish(topic=device + '/' + window + '/set', payload='ON')
                        self.screens['menu'].ids['btn_' + device + '_' + window].background_color = [0, 1, 0, 1]
                else:
                    self.status[device][window]['cnt'] = 0
                    client.publish(topic=device + '/' + window + '/set', payload='OFF')
                    self.screens['menu'].ids['btn_' + device + '_' + window].background_color = [1, 1, 1, 1]
            if self.status[device]['w_open']['cnt'] > 0:
                self.screens['menu'].ids['lb_state_' + device].text = 'Opening... {}'.format(
                    self.status[device]['w_open']['cnt'])
            elif self.status[device]['w_close']['cnt'] > 0:
                self.screens['menu'].ids['lb_state_' + device].text = 'Closing... {}'.format(
                    self.status[device]['w_close']['cnt'])
            else:
                if self.last_cmd[device] == 'w_open':
                    self.screens['menu'].ids['lb_state_' + device].text = 'Open'
                else:
                    self.screens['menu'].ids['lb_state_' + device].text = 'Close'

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()
        if self.confirm_param[0] == 'connect_to_ap':
            ssid = self.confirm_param[1]
            pwd = self.confirm_param[2]
            new_ip = net_util.connect_to_ap(interface='wlan1', ssid=ssid, pwd=pwd)
            if new_ip is not None:
                self.caution_popup.ids.lb_content.text = 'Connected to {}\n New IP: {}'.format(ssid, new_ip)
            else:
                self.caution_popup.ids.lb_content.text = 'Failed to connect'
            self.caution_popup.open()

    def confirm_no(self):
        """
        callback function of "No" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        if dest_screen == 'settings':
            self.update_settings()
        elif dest_screen == 'settings_wifi':
            Clock.schedule_once(self.update_ap_list)

        sm = self.root.ids.sm
        sm.transition = SlideTransition()
        screen = self.screens[dest_screen]
        sm.switch_to(screen, direction=direction)
        self.current_title = screen.name

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob(cur_dir + "screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        available_screens.append('schedule')

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        self.screens['schedule'] = ScheduleScreen()
        return True

    def on_btn(self, device, action):
        """
        Callback function when OPEN/STOP/CLOSE button is pressed
        :param action: OPEN/CLOSE/STOP
        :param device: device name
        :return:
        """
        if action == 'stop':
            for window in self.status[device].keys():
                if self.status[device][window]['cnt'] > 1:
                    self.status[device][window]['cnt'] = 0
                    popup = Popup(title='Churchie-1', title_size=22, size_hint=(.6, .4),
                                  content=Label(text='Stopping {} of {}...'.format(window, device), font_size=20))
                    # popup.open()
                    print_with_time('Stopping {} of {}...'.format(window, device))
                    Clock.schedule_once(partial(self.remove_popup, popup), 5)
        else:
            window = 'w_' + action
            another_window = 'w_open' if window == 'w_close' else 'w_close'
            if self.status[device][window]['cnt'] > 0:
                print 'Error, {} of {} is already running...'.format(window, device)
            elif self.status[device][another_window]['cnt'] > 0:
                # Popup(title='HVB-1', title_size=22, size_hint=(.6, .4),
                #       content=Label(text='Another relay on this device is running...', font_size=20)).open()
                self.status[device][another_window]['cnt'] = 0
                self.cnt = 0
                Clock.schedule_once(partial(self.check_another_window, device, window), 1)
            else:
                self.cnt = 0
                Clock.schedule_once(partial(self.check_another_window, device, window), 1)

    def check_another_window(self, *args):
        device = args[0]
        window = args[1]
        # Wait until another window is turned off...
        another_window = 'w_open' if window == 'w_close' else 'w_close'
        self.status[device][another_window]['cnt'] = 0
        if self.status[device][another_window]['state']:
            if self.cnt > 10:
                error_text = 'Failed to turn {} off of {}'.format(another_window, device)
                Popup(title='HVB-1', title_size=22, size_hint=(.6, .4),
                      content=Label(text=error_text, font_size=20))
                print_with_time(error_text)
                return False
            else:
                self.cnt += 1
                print_with_time('Waiting for another window to be turned off.')
                Clock.schedule_once(partial(self.check_another_window, device, window), 1)
        else:
            # After checking rule, let us turn the corresponding relay on!
            self.start_relay_on(device, window)

    @staticmethod
    def remove_popup(popup, *largs):
        """
        Dismiss given popup after a certain time duration.
        :param popup:
        :param largs:
        :return:
        """
        popup.dismiss()

    def draw_wind_dir(self, direction, *args):
        """
        Draw a round compass indicating wind direction.
        :param direction:
        :return:
        """
        # angle = 2 * pi / 60 * datetime.datetime.now().second
        center = [400, 300]
        # Not sure why I have to multiply 6 with this
        # But measured values are ---     North: 1.05, East: 15, South: 30, West: 45
        angle = 2 * pi / 360 * (direction-1.0) * 6
        panel = self.screens['wind'].ids['panel']
        panel.canvas.clear()
        with panel.canvas:
            Color(0.2, 0.5, 0.2)
            offset_x = 0.15 * panel.width * sin(angle)
            offset_y = 0.15 * panel.width * cos(angle)
            Line(points=[center[0] - offset_x, center[1] - offset_y, center[0] + offset_x, center[1] + offset_y],
                 width=2)

            arrow_1_x = 20 * cos(pi/2 - angle - pi/6)
            arrow_1_y = 20 * sin(pi / 2 - angle - pi / 6)
            Line(points=[center[0] + offset_x, center[1] + offset_y, center[0] + offset_x - arrow_1_x,
                         center[1] + offset_y - arrow_1_y], width=2)
            arrow_2_x = 20 * cos(pi / 2 - angle + pi / 6)
            arrow_2_y = 20 * sin(pi / 2 - angle + pi / 6)
            Line(points=[center[0] + offset_x, center[1] + offset_y, center[0] + offset_x - arrow_2_x,
                         center[1] + offset_y - arrow_2_y], width=2)

        self.publish_wind_dir_text((direction-1.0) * 6)

    def publish_wind_dir_text(self, angle):
        dir_text = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'][int((angle - 22.5)/45)]
        client.publish('window1/wind_dir_str', dir_text)

    def btn_settings(self, tag_name, val):
        """
        Update settings value
        :param tag_name:
        :param val:
        :return:
        """
        self.wind_trip = int(self.get_param_from_xml('WIND_SPEED_TRIP'))
        self.wind_delay = int(self.get_param_from_xml('WIND_DELAY'))
        self.rain_delay = int(self.get_param_from_xml('RAIN_DELAY'))
        if tag_name == 'WIND_SPEED_TRIP':
            self.wind_trip += val
            if self.wind_trip < 0:
                self.wind_trip = 0
            self.set_param_to_xml(tag_name, self.wind_trip)
        elif tag_name == 'WIND_DELAY':
            self.wind_delay += val
            if self.wind_delay < 0:
                self.wind_delay = 0
            self.set_param_to_xml(tag_name, self.wind_delay)
        elif tag_name == 'RAIN_DELAY':
            self.rain_delay += val
            if self.rain_delay < 0:
                self.rain_delay = 0
            self.set_param_to_xml(tag_name, self.rain_delay)
        self.update_settings()

    def update_settings(self):
        """
        Update settings widgets with current values
        :return:
        """
        scr = self.screens['settings']
        scr.ids['lb_wind_speed_trip'].text = str(self.wind_trip)
        scr.ids['lb_wind_delay'].text = str(self.wind_delay)
        scr.ids['lb_rain_delay'].text = str(self.rain_delay)

    def touch_wind(self, *args):
        """
        Check current touched position is for the wind widget
        :return:
        """
        if self.screens['menu'].ids['ly_wind'].collide_point(*args[1].pos) and self.en_wind:
            self.go_screen('wind', 'down')

    def btn_back(self):
        """
        When user presses "<<" button in the pin input screen
        :return:
        """
        pin = self.screens['pin'].ids['txt_pin'].text
        if len(pin) > 0:
            self.screens['pin'].ids['txt_pin'].text = pin[:-1]

    def get_window_name(self, index):
        return str(self.window_name[index])

    def btn_pin(self):
        """
        When the user presses 'ENTER' button in the pin screen
        :return:
        """
        pin = self.screens['pin'].ids['txt_pin'].text
        if len(pin) == 0:
            Popup(title='Notification', title_size=25, size_hint=(.6, .4),
                  content=Label(text='Please input PIN CODE.', font_size=25)).open()
            return False

        if self.screens['pin'].ids['button_pin'].text == 'REGISTER':
            self.set_param_to_xml('PIN', pin)
            Popup(title='Notification', title_size=25, size_hint=(.6, .4),
                  content=Label(text='PIN CODE is updated.', font_size=25)).open()
            self.screens['pin'].ids['button_pin'].text = 'ENTER'
            self.screens['pin'].ids['txt_pin'].text = ''
            self.go_screen('menu', 'up')
        else:
            if pin == self.get_param_from_xml('MASTER_PIN'):
                self.screens['pin'].ids['txt_pin'].text = ''
                Popup(title='Notification', title_size=25, size_hint=(.6, .4),
                      content=Label(text='Please register your PIN CODE.', font_size=25)).open()
                self.screens['pin'].ids['button_pin'].text = 'REGISTER'
                return True
            else:
                self.screens['pin'].ids['txt_pin'].text = ''
                if pin != self.get_param_from_xml('PIN'):
                    Popup(title='Error', title_size=25, size_hint=(.6, .4),
                          content=Label(text='Invalid PIN CODE.', font_size=25)).open()
                else:
                    self.go_screen('settings', 'up')

    def show_adv(self, *args):
        if self.screens['menu'].ids['lb_ads'].x > -550:
            self.screens['menu'].ids['lb_ads'].x -= 5
        else:
            self.screens['menu'].ids['lb_ads'].x = 50

    def set_param_to_xml(self, tag_name, new_val):
        if not isinstance(new_val, basestring):
            new_val = str(new_val)
        et = xml.etree.ElementTree.parse(self.conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name)
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break

        return tmp

    def count_down(self, *args):
        global counter
        if counter > 0:
            self.turn_bright(255)
            self.root.ids.img_logo_full.opacity = 0
            counter -= 1
        else:
            print_with_time('Changing brightness to half')
            self.root.ids.img_logo_full.opacity = 1
            self.turn_bright(100)
        # print counter

    def on_sensor_setting(self, widget, sensor, new_val):
        if widget.active:
            print_with_time('Setting {} to be {}d'.format(sensor, new_val))
            self.set_param_to_xml(sensor.upper(), new_val)
            if sensor == 'rain':
                self.en_rain = True if new_val == 'enable' else False
                self.screens['menu'].ids['lb_rain'].opacity = 1 if self.en_rain else 0
            elif sensor == 'wind':
                self.en_wind = True if new_val == 'enable' else False
                self.screens['menu'].ids['ly_wind'].opacity = 1 if self.en_wind else 0
            else:
                self.en_temp = True if new_val == 'enable' else False
                self.screens['menu'].ids['bl_temp'].opacity = 1 if self.en_temp else 0
                # if not self.en_temp:
                #     self.screens['menu'].ids['lb_temp_in'].text = '[color=A0A0A0]disabled[/color]'
                # else:
                #     self.screens['menu'].ids['lb_temp_in'].text = ''

    def update_window_names(self):
        for i in range(4):
            self.window_name[i] = self.get_param_from_xml('WINDOW{}'.format(i))

    def update_sensor_settings(self):
        for sensor in ['rain', 'wind', 'temp']:
            val = self.get_param_from_xml(sensor.upper())
            self.screens['settings'].ids['chk_{}_{}'.format(sensor, val)].active = True
            if sensor == 'rain':
                self.en_rain = True if val == 'enable' else False
                self.screens['menu'].ids['lb_rain'].opacity = 1 if self.en_rain else 0
            elif sensor == 'wind':
                self.en_wind = True if val == 'enable' else False
                self.screens['menu'].ids['ly_wind'].opacity = 1 if self.en_wind else 0
            else:
                self.en_temp = True if val == 'enable' else False
                self.screens['menu'].ids['bl_temp'].opacity = 1 if self.en_temp else 0
                # if not self.en_temp:
                #     self.screens['menu'].ids['lb_temp_in'].text = '[color=A0A0A0]disabled[/color]'
            # if not self.en_wind:
            #     self.screens['menu'].ids.lb_wind_speed.text = 'disabled'
            #     self.screens['menu'].ids.lb_wind_speed.color = [.6, .6, .6, 1]
            # else:
            #     self.screens['menu'].ids.lb_wind_speed.color = [1, 1, 1, 1]

    @staticmethod
    def open_window_dialog():
        WindowPopup().open()

    def apply_window_names(self, popup):
        popup.dismiss()
        for i in range(4):
            new_val = popup.ids['txt_window{}'.format(i)].text
            self.set_param_to_xml('WINDOW{}'.format(i), new_val)
            self.screens['menu'].ids['lb_window_name_{}'.format(i)].text = new_val

    @staticmethod
    def turn_bright(n, *args):
        """
        Adjust brightness of touch screen
        :param n: brightness value, range 0 ~ 255
        :param args:
        :return:
        """
        if platform.system() != 'Windows':
            os.system('sudo echo {} > /sys/class/backlight/rpi_backlight/brightness'.format(n))

    def check_schedule(self, *args):
        with open('config.xml', 'r') as fd:
            _xml = xmltodict.parse(fd.read())
        cur_day = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'][datetime.datetime.today().weekday()]
        for i in range(1, 5):
            schedule = _xml['CONFIG']['SCHEDULE']['WINDOW{}'.format(i)]
            if schedule[cur_day]['ACTIVE'] == 'ON':
                if schedule[cur_day]['START'] == datetime.datetime.now().strftime('%H:%M'):
                    self.on_btn('window{}'.format(i), 'open')
                elif schedule[cur_day]['STOP'] == datetime.datetime.now().strftime('%H:%M'):
                    self.on_btn('window{}'.format(i), 'close')

    def btn_connect_to_ap(self):
        """
        This function is called when user presses 'Connect' button.
        :return:
        """
        pwd = self.screens['settings_wifi'].ids['txt_pwd'].text
        ssid = self.screens['settings_wifi'].ids['btn_dropdown'].text
        self.confirm_popup.ids['lb_confirm'].text = 'Connect to ' + ssid + "?\nIt will take a while"
        self.confirm_param[0] = 'connect_to_ap'
        self.confirm_param[1] = ssid
        self.confirm_param[2] = pwd
        self.confirm_popup.open()

    def show_time(self, *args):
        _time = datetime.datetime.now()
        str_time = _time.strftime("%B %d, %Y  %H:%M:%S")
        self.screens['menu'].ids.lb_time.text = '{}, {}'.format(calendar.day_name[_time.weekday()][:3], str_time)


def print_with_time(msg):
    print datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S :  "), msg


if __name__ == '__main__':

    app = MainApp()
    app.run()
