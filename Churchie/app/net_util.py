"""
    Utility module related to net
"""
import os
import random
import socket
import struct
import subprocess

import wifi
import netifaces
# from aware_logger import aware_logger
from kivy.config import _is_rpi

try:
    import fcntl
except ImportError:
    pass


def get_current_ap(interface='wlan0'):
    """
    Get currently connected AP's SSID
    :return:
    """
    if _is_rpi:
        pipe = os.popen('iwconfig | grep {}'.format(interface))
        lines = pipe.read().strip().splitlines()
        pipe.close()
        ap = ''
        for line in lines:
            if line.startswith(interface):
                ap = line.split('"')[-2]
                break
    else:
        ap = 'Testing AP'
        # ap = None
    return ap


def get_current_interface():
    """
    Get currently connected wifi interface name
    :return:
    """
    if _is_rpi:
        pipe = os.popen('iwgetid')
        interface = pipe.read().split()[0]
        pipe.close()
    else:
        interface = 'wlan1'
        # ap = None
    return interface


def get_ip_address(ifname='wlan1'):
    """
    Get assigned IP address of given interface
    :param ifname: interface name such as wlan0, eth0, etc
    :return:
    """
    if _is_rpi:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
            return ip
        except IOError:
            pass
        except struct.error:
            pass
    else:
        return '192.168.1.110'


def get_ap_list(interface='wlan1'):
    ap_list = []
    if _is_rpi:
        try:
            l_cell = wifi.Cell.all(interface)
        except wifi.exceptions.InterfaceError:
            # When wlan0 is down, turn it on and scan again.
            subprocess.call(['/sbin/ifup {}'.format(interface)], shell=True)
            l_cell = wifi.Cell.all(interface)

        for cell in l_cell:
            str_q = cell.quality.split('/')     # quality: '43/70' or '68/70' or so
            quality = int(float(str_q[0]) / float(str_q[1]) * 100)  # We need percentage value
            ap_list.append(cell.ssid)
        return ap_list
    else:
        name_list = ['Dummy ssid {}'.format(i) for i in range(10)]
        name_list += ['Testing AP']
        for ap_name in name_list:
            ap_list.append(ap_name)
        return ap_list


def get_wifi_strength():
    """
    Get wireless signal strength
    :return: Percentage value
    """
    if _is_rpi:
        pipe = os.popen('cat /proc/net/wireless')
        data = pipe.read().strip().splitlines()
        pipe.close()
        try:
            strength = data[2].split()[2][:-1]
        except IndexError:
            # print 'Failed to get current wifi strength, setting it to 0'
            strength = 0
        return round(float(strength) / 70.0 * 100, 2)
    else:
        return random.randint(0, 100)


def connect_to_ap(interface='wlan1', ssid='', pwd=''):
    """
    Connect to AP and return new assigned IP address
    :param interface:
    :param ssid:
    :param pwd:
    :return: IP address if success, otherwise None
    """
    # aware_logger.info('WiFi: Connecting to {}'.format(ssid))
    if _is_rpi:
        # ---- modify /etc/network/interface file ----
        f = open('/etc/network/interfaces', 'r+b')
        f_content = f.readlines()
        line_num = 0
        for i in range(len(f_content)):
            if {'iface', 'inet', interface}.issubset(f_content[i].split()):
                line_num = i
                break
        f_content[line_num] = 'iface {} inet dhcp\n'.format(interface)
        try:
            f_content[line_num + 1] = '    wpa-ssid ' + ssid + '\n'
        except IndexError:
            f_content.append('    wpa-ssid ' + ssid + '\n')

        try:
            f_content[line_num + 2] = '    wpa-psk ' + pwd + '\n'
        except IndexError:
            f_content.append('    wpa-psk ' + pwd + '\n')

        for i in range(len(f_content)):
            if 'wpa-conf /etc/wpa' in f_content[i] and '#' not in f_content[i]:
                f_content[i] = "#  " + f_content[i]

        f.seek(0)
        f.truncate()
        f.write(''.join(f_content))
        f.close()
        os.system('/usr/bin/sudo /sbin/ifdown {}'.format(interface))
        os.system('/usr/bin/sudo /sbin/ifup {}'.format(interface))

        ip = get_ip_address(interface)
        return ip


def get_detail_of_connected_net(interface='wlan1'):
    """
    Get detail of connected network interface
    :param interface: `wlan0` in default
    :return:
    """
    ap_name = get_current_ap()
    strength = get_wifi_strength()
    ip = get_ip_address(interface)
    mac_addr = get_mac_address(interface)
    return {
        'ssid': ap_name,
        'quality': strength,
        'ip': ip,
        'mac': mac_addr
    }


def get_mac_address(interface='wlan1'):
    try:
        mac_str = open('/sys/class/net/' + interface + '/address').read()
    except Exception as e:
        # aware_logger.error('Failed to get MAC address of {}: {}'.format(interface, e))
        mac_str = "00:00:00:00:00:00"
    return mac_str[0:17]


def get_eth_settings():
    """
    Retrieve current ethernet settings
    """
    if _is_rpi:
        ip4 = netifaces.ifaddresses('eth0')[2][0]
        ip4['gateway'] = get_gateway('eth0')
        ip4['network'] = get_router_address('eth0')
        ip4['mode'] = get_network_mode('eth0')
    else:
        ip4 = {'broadcast': '192.168.1.255', 'netmask': '255.255.255.0', 'addr': '192.168.1.106',
               'gateway': '192.168.1.1', 'network': '192.168.1.0', 'mode': 'DHCP'}

    return ip4


def set_eth_settings(mode='dhcp', ip='192.168.1.110', netmask='255.255.255.0',
                     gateway='192.168.1.254', broadcast='192.168.1.255', network='192.168.1.0'):
    """
    Change ethernet settings
    :param netmask: Subnet Mask Address
    :param network: Router IP
    :param broadcast: Broadcast IP Range
    :param gateway:
    :param mode: `dhcp` or `static`
    :param ip: IP address to be set
    """
    # print 'Applying new ethernet settings', mode, ip, netmask, gateway, broadcast, network
    if _is_rpi:
        # ---- modify /etc/network/interface file ----
        f = open('/etc/network/interfaces', 'r+b')
    else:
        f = open('test_net.txt', 'r+b')

    f_content = f.readlines()
    line_num = -1
    for i in range(len(f_content)):
        if {'iface', 'inet', 'eth0'}.issubset(f_content[i].split()):
            line_num = i
            break
    if line_num >= 0:
        f_content[line_num] = 'iface eth0 inet {}\n'.format(mode)
    else:
        f_content.append('iface eth0 inet {}\n'.format(mode))
        line_num = len(f_content)

    data = {'address': ip, 'netmask': netmask, 'gateway': gateway, 'broadcast': broadcast, 'network': network}
    i = 1
    while mode != 'dhcp':
        try:
            _key = f_content[line_num + i].strip()
            try:
                _key = _key.split()[0]
            except IndexError:
                _key = ''
            if _key in data.keys():
                f_content[line_num + i] = '    ' + _key + ' ' + data.pop(_key) + '\n'
            else:
                _key = data.keys()[0]
                f_content.insert(line_num + 1, '    ' + _key + ' ' + data.pop(_key) + '\n')
        except IndexError:
            _key = data.keys()[0]
            f_content.append('    ' + _key + ' ' + data.pop(_key) + '\n')

        if len(data.keys()) == 0:
            break
        else:
            i += 1

    f.seek(0)
    f.truncate()
    f.write(''.join(f_content))
    f.close()

    if _is_rpi:
        os.system('ip link set eth0 down')
        os.system('ip link set eth0 up')

        ip = get_ip_address('eth0')
        return ip


def get_gateway(interface='wlan1'):
    """
    Get gateway address
    """
    gateways = netifaces.gateways()[2]
    try:
        gateway = [g[0] for g in gateways if g[1] == interface and g[2]][0]
    except IndexError:
        gateway = '0.0.0.0'
    return gateway


def get_router_address(interface='wlan0'):
    """
    Get router address of a interface
    """
    pipe = os.popen("netstat -nr")
    data = pipe.read().strip().splitlines()
    router_addr = '0.0.0.0'
    for r in data:
        sp = r.split()
        if sp[-1] == interface and sp[3] == 'U':
            router_addr = sp[0]
            break
    return router_addr


def get_network_mode(interface='wlan0'):
    """
    Get current networking mode
    :return: `dhcp`, `static`, `manual`
    """
    if _is_rpi:
        f = open('/etc/network/interfaces', 'r+b')
        f_content = f.readlines()
        f.close()
        mode = 'dhcp'
        for line in f_content:
            if {'iface', 'eth0', 'inet'}.issubset(line.split()):
                mode = line.split()[-1]
                break
        return mode.upper()
    else:
        return 'DHCP'


if __name__ == '__main__':
    set_eth_settings('static')
