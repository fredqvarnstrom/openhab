#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* device_name = "Office";   // Change this value for each WiFi devices

int upload_interval = 1000;       // Uploading interval of DHT22 data in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

const char* temp_suffix = "/temperature";
const char* hum_suffix = "/humidity";

const char* w_open_set_suffix = "/w_open/set";
const char* w_open_state_suffix = "/w_open/state";

const char* w_close_set_suffix = "/w_close/set";
const char* w_close_state_suffix = "/w_close/state";

const char* rain_state_suffix = "/rain";

// Include and Configure DHT11 SENSOR
#include "DHT.h"
#define DHTPIN 13     // D7, GPIO13
#define DHTTYPE DHT22   // DHT 22

int PIN_W_OPEN = 5;    // Use D1, GPIO5
int PIN_W_CLOSE = 4;    // Use D2, GPIO4

int PIN_RAIN_SENSOR = 14;   // Use D5, GPIO14

// Global variables
DHT dht(DHTPIN, DHTTYPE);

float humidity, temp_c, temp_f, heatindex;

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;
long t_start_open = 0;      // start time of windows open
long t_start_close = 0;     // start time of windows close
const char* w_open_state = "OFF";
const char* w_close_state = "OFF";

char buf_pub_topic[50];
char buf_sub_topic[50];

void setup() {
  pinMode(PIN_W_OPEN, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(PIN_W_OPEN, LOW);
  pinMode(PIN_W_CLOSE, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(PIN_W_CLOSE, LOW);
  
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");
  
  // Check topic of windows open relay
  set_sub_topic(w_open_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    if (!strncmp((const char*)payload, "ON", 2)) {
//      if (!strncmp(w_close_state, "ON", 2)) {
//        Serial.println("WINDOWS CLOSE is already on, try again later...");
//      }
//      else{
        w_open_state = "ON";
        Serial.println("Turning RELAY of WINDOWS OPEN ON...");
        digitalWrite(PIN_W_OPEN, HIGH);     
//        t_start_open = millis();
//      }
    } else if (!strncmp((const char*)payload, "OFF", 3)){
      w_open_state = "OFF";
      Serial.println("Turning RELAY of WINDOWS OPEN OFF...");
      digitalWrite(PIN_W_OPEN, LOW);  
    }
  }   
  // Check topic of windows close relay
  else{
    set_sub_topic(w_close_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
      if (!strncmp((const char*)payload, "ON", 2)) {
//        if (!strncmp(w_open_state, "ON", 2)) {
//          Serial.println("WINDOWS OPEN is already on, try again later...");
//        }
//        else{
            w_close_state = "ON";
            Serial.println("Turning RELAY of WINDOWS CLOSE ON...");
            digitalWrite(PIN_W_CLOSE, HIGH);   
//            t_start_close = millis();
//        }
      } else if (!strncmp((const char*)payload, "OFF", 3)){
        w_close_state = "OFF";
        Serial.println("Turning RELAY of WINDOWS CLOSE OFF...");
        digitalWrite(PIN_W_CLOSE, LOW);  
      }
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(w_open_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(w_close_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  // turn relays off after 90 sec
//  if ((millis() - t_start_open > 90 * 1000) && !strncmp(w_open_state, "ON", 2)){
//    w_open_state = "OFF";
//    digitalWrite(PIN_W_OPEN, LOW);  
//  }
//  if ((millis() - t_start_close > 90 * 1000) && !strncmp(w_close_state, "ON", 2)){
//    w_close_state = "OFF";
//    digitalWrite(PIN_W_CLOSE, LOW);  
//  }
  
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > upload_interval) {
    lastMsg = now;
    ReadDHT();
    char* buf_temp = new char[10];
    char* buf_hum = new char[10];

    dtostrf(temp_c, 5, 2, buf_temp);
    dtostrf(humidity, 5, 2, buf_hum);

    // publish temperature value
    set_pub_topic(temp_suffix);
    client.publish(buf_pub_topic, buf_temp);

    // publish humidity value
    set_pub_topic(hum_suffix);
    client.publish(buf_pub_topic, buf_hum);

    // publish state of window open relay 
    set_pub_topic(w_open_state_suffix);
    client.publish(buf_pub_topic, w_open_state);

    // publish state of window close relay
    set_pub_topic(w_close_state_suffix);
    client.publish(buf_pub_topic, w_close_state);

    // publish state of rain sensor
    set_pub_topic(rain_state_suffix);
    if (digitalRead(PIN_RAIN_SENSOR) == LOW)
      client.publish(buf_pub_topic, "OFF");
    else
      client.publish(buf_pub_topic, "ON");
  }
  else{
    delay(400);   // Loop function takes about 300ms, so 400 ms is enough.
  }
}

void ReadDHT() {
  // Read humidity (percent)
  humidity = dht.readHumidity();
  // Read temperature as Celsius
  temp_c = dht.readTemperature();
  // Read temperature as Fahrenheit
//  temp_f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(humidity) || isnan(temp_c)) {
    Serial.println("Failed to read from DHT sensor :-(");
    return;
  }

  // Compute heat index
  // Must send in temp in Fahrenheit!
//  heatindex = dht.computeHeatIndex(temp_f, humidity);

  Serial.print("Temp:  ");
  Serial.print(temp_c);
  Serial.print(",  Humidity:");
  Serial.println(humidity);
  
}

void set_pub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}
