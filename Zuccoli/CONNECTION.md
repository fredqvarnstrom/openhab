# Pin Connection

| **GPIO #**  |    **PIN #**  | **Description**         |
|    ----     |    :-------:  |     :-------:           |
|    4        |     D2        | **SDA** of **ADS7830**  |
|    5        |     D1        | **SCL** of **ADS7830**  |
|    15       |     D8        |         DHT22_Data      |
|    12       |     D6        |         uRelay_1        |
|    13       |     D7        |         uRelay_2        |


## Moved pins

- `uRelay_1(D1/GPIO5)` => `D6(GPIO12)`
- `uRelay_2(D2/GPIO4)` => `D7(GPIO13)`
- `DHT22_Data(D0/GPIO16)` => `D8(GPIO15)`
