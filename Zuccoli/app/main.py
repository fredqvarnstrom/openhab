# -*- coding: utf-8 -*-
import calendar
import datetime
import gc
import glob
import os
import threading
import time
import xml.etree.ElementTree
from functools import partial
from math import cos, sin, pi


def is_rpi():
    try:
        return 'arm' in os.uname()[4]
    except AttributeError:
        return False


if is_rpi():
    os.environ['KIVY_GL_BACKEND'] = 'gl'

import paho.mqtt.client as mqtt
import xmltodict
from kivy.app import App
from kivy.clock import Clock, mainthread
from kivy.core.text import LabelBase
from kivy.graphics import Color, Line
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, ListProperty, ObjectProperty, NumericProperty
from kivy.uix.dropdown import DropDown
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition, ScreenManagerException

import logger
import net_util
from time_util import update_system_datetime
from popups.popup import ScreenSaverPopup, WindowPopup, AfterHoursPopup, TimeSchedulePopup
from screens.schedule.screen import ScheduleScreen
from settings import MQTT_HOST, WINDOW_CNT, EN_OPENHAB, INIT_SCREEN
from widgets.datetime.calendar.calendar_ui import DatePicker
from widgets.datetime.time_picker import TimePicker


# from widgets.snackbar import show_info_snackbar


def is_rpi():
    return os.path.exists('/opt/vc/include/bcm_host.h')


client = mqtt.Client()
cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
counter = 60 * 5        # Keep brightness for 5 min
LabelBase.register(name="Icon", fn_regular="assets/fonts/materialdesignicons-webfont.ttf")


class MainWidget(FloatLayout):

    def on_touch_down(self, touch):
        global counter
        _app = App.get_running_app()
        if counter == 0:
            counter = int(_app.get_param_from_xml('SCREEN_ACTIVE')) * 60
            Clock.schedule_once(_app.count_down)
            return
        counter = int(_app.get_param_from_xml('SCREEN_ACTIVE')) * 60
        super(MainWidget, self).on_touch_down(touch)
        if _app.current_title == 'menu':
            if _app.screens['menu'].ids.img_client_logo.collide_point(*touch.pos):
                _app.go_screen('pin' if is_rpi() else 'settings', 'left')


class MainApp(App):
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty()
    screens = {}                            # Dict of all screens
    hierarchy = ListProperty()

    conf_file_name = cur_dir + 'config.xml'
    wind_trip = NumericProperty(10)
    wind_delay = NumericProperty(5)
    rain_delay = NumericProperty(4)
    sec_close_after_fire = 10

    b_wind_trip = BooleanProperty(False)
    b_rain = BooleanProperty(False)
    b_close_all = BooleanProperty(False)
    status = ObjectProperty(None)

    last_cmd = ObjectProperty(None)

    cnd = NumericProperty(0)

    cur_window = NumericProperty(1)
    dropdown = DropDown()  # DropDown instance in 'WiFi Setting' screen

    en_rain = BooleanProperty(False)
    en_wind = BooleanProperty(False)
    en_temp = BooleanProperty(False)
    en_time_schedule = BooleanProperty(True)
    is_aft_hr = BooleanProperty(False)

    window_name = ListProperty()

    window_status = [False, ] * (WINDOW_CNT + 1)

    is_running_schedule = [False, ] * (WINDOW_CNT + 1)
    event_after_hours = [None, ] * (WINDOW_CNT + 1)

    air_state = [False, ] * (WINDOW_CNT + 1)
    air_message = [False, ] * (WINDOW_CNT + 1)

    event_after_fire = False

    screen_saver = None

    last_time = [time.time(), ] * (WINDOW_CNT + 1)

    last_fire_opened_time = NumericProperty(-1)
    cnt_fire = NumericProperty(0)
    last_security_closed_time = NumericProperty(-1)
    cnt_security = NumericProperty(0)
    last_air_con_closed_time = [-1] * (WINDOW_CNT + 1)
    cnt_air_con = [0] * (WINDOW_CNT + 1)
    msg_cnt = 0

    def __init__(self, **kwargs):
        global counter
        counter = int(self.get_param_from_xml('SCREEN_ACTIVE')) * 60
        super(MainApp, self).__init__(**kwargs)
        self.cnt = 0
        self.window_name = [''] * WINDOW_CNT

        self.inner_num = self.get_param_from_xml('INNER_TEMP_WINDOW_NUM')
        self.outer_num = self.get_param_from_xml('OUTER_TEMP_WINDOW_NUM')
        self.rain_num = self.get_param_from_xml('RAIN_WINDOW_NUM')
        self.wind_num = self.get_param_from_xml('WIND_WINDOW_NUM')
        self.security_num = self.get_param_from_xml('SECURITY_WINDOW_NUM')
        self.operation_time = int(self.get_param_from_xml('WINDOW_OPERATION_TIME'))
        self.fire_enabled = [self.get_param_from_xml('FIRE_{}'.format(i)) == 'ON' for i in range(WINDOW_CNT)]
        self.fire_status = [False for _ in range(WINDOW_CNT + 1)]
        self.air_enabled = [self.get_param_from_xml('AIR_{}'.format(i)) == 'ON' for i in range(WINDOW_CNT)]
        self.date_picker = DatePicker(size_hint=(None, None), width=140, height=40, font_size=24)
        self.time_picker = TimePicker(size_hint=(None, None), width=80, height=40, font_size=24)

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self.load_screen()
        self.status = dict()
        self.last_cmd = {}
        for device in ['window{}'.format(i) for i in range(1, WINDOW_CNT + 1)]:
            self.status[device] = {
                'w_open': {'cnt': 0, 'state': None},
                'w_close': {'cnt': 0, 'state': None},
            }
            self.last_cmd[device] = ''

        self.status['rain'] = {'state': None, 'time': -1}
        self.status['wind'] = {'state': None, 'time': -1}

        self.wind_trip = int(self.get_param_from_xml('WIND_SPEED_TRIP'))
        self.wind_delay = int(self.get_param_from_xml('WIND_DELAY'))
        self.rain_delay = int(self.get_param_from_xml('RAIN_DELAY'))
        self.sec_close_after_fire = int(self.get_param_from_xml('CLOSE_AFTER_FIRE'))
        self.show_after_hours_label()
        self.update_air_cond_label()
        self.update_sensor_settings()
        self.screen_saver = ScreenSaverPopup()

        self.go_screen(INIT_SCREEN, 'right')

        Clock.schedule_interval(lambda dt: self.execute_relay(), 1)
        Clock.schedule_interval(self.count_down, 1)
        Clock.schedule_interval(self.check_schedule, 1)
        Clock.schedule_interval(lambda dt: self.check_automatic_time_schedule(), 1)
        Clock.schedule_interval(self.show_time, 1)
        Clock.schedule_interval(self.check_activity, 1)

        client.on_connect = self.on_connect
        client.on_message = self.on_message
        client.connect(MQTT_HOST, 1883, 60)

        # Clock.schedule_interval(self.draw_wind_dir, 1)
        threading.Thread(target=client.loop_forever).start()
        self._update_openhab_sitemap()

    @staticmethod
    def on_connect(_client, userdata, flags, rc):
        """
        The callback for when the client receives a CONNECT response from the server.
        :return:
        """
        logger.info("Connected with result code " + str(rc))

        for i in range(1, WINDOW_CNT + 1):
            client.subscribe('window{}/w_open/set'.format(i))
            client.subscribe("window{}/w_open/state".format(i))
            client.subscribe("window{}/w_close/set".format(i))
            client.subscribe("window{}/w_close/state".format(i))
            client.subscribe('window{}/fire/state'.format(i))
            client.subscribe('window{}/air/state'.format(i))
            client.subscribe('window{}/security_close/state'.format(i))
            client.subscribe('window{}/after_hours/state'.format(i))
            client.subscribe('window{}/temperature'.format(i))
            client.subscribe('window{}/rain'.format(i))
            client.subscribe('window{}/wind_speed'.format(i))
            client.subscribe('window{}/wind_dir'.format(i))
        client.subscribe('window/open_all')
        client.subscribe('window/close_all')

    @mainthread
    def on_message(self, _client, userdata, msg):
        """
        The callback for when a PUBLISH message is received from the server.
        :param _client:
        :param userdata:
        :param msg:
        :return:
        """
        topic = str(msg.topic)
        message = msg.payload
        if message == 'nan':
            return False

        # s_time = time.time()

        # print 'Topic: {}, message: {}'.format(topic, message)
        scr = self.screens['menu']

        if topic == 'window/open_all':
            self.open_all_windows()
            return
        elif topic == 'window/close_all':
            self.close_all_windows()
            return

        win_num = int(topic[6])
        self.last_time[win_num] = time.time()

        # temperature topics
        if topic == 'window{}/temperature'.format(self.inner_num):
            if self.en_temp:
                logger.info('Updating Inner Temperature - {}'.format(message))
                scr.ids['lb_temp_in'].text = '{} °C'.format(message)
                client.publish(topic='window/temperature_in', payload=message)
        elif topic == 'window{}/temperature'.format(self.outer_num):
            if self.en_temp:
                logger.info('Updating Outer Temperature - {}'.format(message))
                scr.ids['lb_temp_out'].text = '{} °C'.format(message)
                client.publish(topic='window/temperature_out', payload=message)

        # rain topic
        elif topic == 'window{}/rain'.format(self.rain_num) and self.en_rain:
            if message == 'ON':
                scr.ids['lb_rain'].text = '[b][color=408000]RAIN - YES[/color][/b]'
                self.on_rain_event(True)
            else:
                scr.ids['lb_rain'].text = '[b][color=408000]RAIN - NO[/color][/b]'
                self.on_rain_event(False)
            client.publish(topic='window/rain', payload=message)

        # wind topics
        elif topic == 'window{}/wind_speed'.format(self.wind_num) and self.en_wind:
            self.on_wind_speed_event(float(message))
            client.publish(topic='window/wind_speed', payload=message)

        elif topic == 'window{}/wind_dir'.format(self.wind_num) and self.en_wind:
            self.draw_wind_dir(float(message))

        elif topic.endswith('/fire/state'):
            if not self.fire_enabled[win_num - 1]:
                logger.info('`Fire` value({}) is detected, but window{} is not enabled.'.format(message, win_num))
                return
            if message == 'ON':
                self.fire_status[win_num] = True
                delay = self.operation_time + 6
                if self.last_fire_opened_time < 0 or \
                        (time.time() - self.last_fire_opened_time > delay and self.cnt_fire < 3):
                    if self.event_after_fire:
                        self.event_after_fire.cancel()
                    self.event_after_fire = None
                    logger.info('!!! Warning, `FIRE` is detected from Window {}! - {}'.format(win_num, self.cnt_fire))
                    if self.event_after_hours[win_num] is None:
                        # Create virtual event to close window afterwards
                        self.event_after_hours[win_num] = Clock.schedule_once(
                                partial(self.on_btn, 'window{}'.format(win_num), 'close'), 120 * 60 * 6000)
                    self.event_after_hours[win_num].cancel()
                    self.screens['menu'].ids.lb_fire.opacity = 1
                    self.cnt_fire += 1
                    self.last_fire_opened_time = time.time()
                    self.open_all_windows()
            else:
                self.fire_status[win_num] = False

            if not any(self.fire_status):
                self.cnt_fire = 0
                self.last_fire_opened_time = -1
                self.screens['menu'].ids.lb_fire.opacity = 0
                if self.event_after_fire is None:
                    self.event_after_fire = Clock.schedule_once(
                        lambda dt: self.close_after_fire(), self.sec_close_after_fire)

        elif topic.endswith('/air/state'):
            if not self.air_enabled[win_num - 1]:
                logger.info('`Air` value({}) is detected, but window{} is not enabled.'.format(message, win_num))
                return
            if message == 'ON':
                delay = self.operation_time + 6
                if self.event_after_fire is None:
                    logger.info('`AIR` is triggered on window{}, but `AfterFire` is running...'.format(win_num))
                else:
                    if self.last_air_con_closed_time[win_num] < 0 or \
                            (time.time() - self.last_air_con_closed_time[win_num] > delay and
                             self.cnt_air_con[win_num] < 3):
                        logger.info(
                            'Warning, `AIR` of window{} is detected! - {}'.format(win_num, self.cnt_air_con[win_num]))
                        self.cancel_after_hours(win_num)
                        self.cnt_air_con[win_num] += 1
                        self.last_air_con_closed_time[win_num] = time.time()
                        self.on_btn('window{}'.format(win_num), self.get_param_from_xml("AIR_COND_ACTION").lower())
                        self.air_state[win_num] = True
            else:
                if self.last_air_con_closed_time[win_num] > 0:
                    logger.info('AIRCON is just removed')
                self.air_state[win_num] = False
                self.cnt_air_con[win_num] = 0
                self.last_air_con_closed_time[win_num] = -1
            self.air_message[win_num] = True if message == 'ON' else False
            self.screens['menu'].ids.lb_air_cond.text = 'Air: {}'.format('ON' if any(self.air_message) else 'OFF')

        elif topic.endswith('/after_hours/state'):
            if message == 'ON':
                if self.air_state[win_num]:
                    logger.info('`AfterHours` of window{} is triggered, but `AIR` is running..'.format(win_num))
                else:
                    _type = self.get_param_from_xml('AFTER_HOURS_{}'.format(win_num))
                    if _type == 'OFF':
                        logger.info('`AfterHours` of window{} is triggered, but this is disabled'.format(win_num))
                    elif _type == 'LOCAL':
                        if self.window_status[win_num - 1]:
                            logger.info('LOCAL `AfterHours` is triggered on window{}'.format(win_num))
                            self.perform_after_hours(win_num)
                    elif _type == 'GLOBAL':
                        logger.info('GLOBAL `AfterHours` is triggered on window{}'.format(win_num))
                        for i in range(1, WINDOW_CNT + 1):
                            if self.window_status[i - 1]:
                                self.perform_after_hours(i)

        elif topic.endswith('/security_close/state'):
            if win_num == int(self.security_num):
                self.screens['menu'].ids.lb_security.opacity = 1 if message == 'ON' else 0
                if message == 'ON':
                    delay = self.operation_time + 6
                    if self.last_security_closed_time < 0 or \
                            (time.time() - self.last_security_closed_time > delay and self.cnt_security < 3):
                        logger.info('WARNING! SECURITY_CLOSE on Window{} is detected! - {}'.format(
                            self.security_num, self.cnt_security))
                        self.cnt_security += 1
                        self.last_security_closed_time = time.time()
                        self.close_all_windows()
                else:
                    if self.last_security_closed_time > 0:
                        logger.info('SECURITY_CLOSE on Window{} is removed'.format(self.security_num))
                    self.cnt_security = 0
                    self.last_security_closed_time = -1

        # status topics
        elif '/state' in topic:
            # sample topic: "window1/w_open/state"
            [device, window, _] = topic.split('/')
            self.status[device][window]['state'] = True if message == 'ON' else False

        elif '/set' in topic:
            # sample topic: "window1/w_open/set"
            self.on_event_from_openhab(topic, message)

        # print 'Parsed MQTT message(`{}` - `{}`) - {}, {}'.format(topic, message, self.msg_cnt, time.time() - s_time)
        # self.msg_cnt += 1

    def perform_after_hours(self, win_num):
        is_window_running = any([self.status['window{}'.format(win_num)][w]['cnt'] > 0
                                 for w in ['w_open', 'w_close']])

        def _check_normal_time(num):
            """
            Check if current time is normal operating time for a window
            :param num: Window number
            :return:
            """
            with open('config.xml', 'r') as fd:
                _xml = xmltodict.parse(fd.read())
            sch = _xml['CONFIG']['AUTOMATIC_TIME_SCHEDULE']
            if sch['TYPE'] == 'GLOBAL':
                start_time = sch['GLOBAL_OPEN']
                end_time = sch['GLOBAL_CLOSE']
            else:
                s = sch['WINDOW{}'.format(num)]
                if s['ACTIVE'] == 'OFF':
                    return False
                start_time = s['OPEN']
                end_time = s['CLOSE']
            return datetime.datetime.strptime(start_time, "%H:%M").time() < datetime.datetime.now(
                        ).time() < datetime.datetime.strptime(end_time, "%H:%M").time()

        if self.event_after_hours[win_num] is None:  # Not started yet
            if _check_normal_time(win_num):
                logger.info('It\'s the normal operating time for window{}, just OPEN/STOP the window'.format(win_num))
                if not is_window_running:
                    self.event_after_hours[win_num] = Clock.schedule_once(
                        partial(self.on_btn, 'window{}'.format(win_num), 'open'))
                    # show_info_snackbar("Just opened Window{}".format(win_num))
                elif self.status['window{}'.format(win_num)]['w_open']['cnt'] > 0:
                    # When it was opening, stop it, and declare event instance to close window next time
                    self.event_after_hours[win_num] = Clock.schedule_once(
                        partial(self.on_btn, 'window{}'.format(win_num), 'stop'))
                else:
                    # When it was closing, just stop it, and it will open next time
                    self.on_btn('window{}'.format(win_num), 'stop')
                self.is_aft_hr = False
            else:
                msg = 'Schedule of window{} is not running, OPEN/STOP the window '.format(win_num)
                if is_window_running:
                    self.on_btn('window{}'.format(win_num), 'stop')
                else:
                    self.on_btn('window{}'.format(win_num), 'open')
                if self.en_time_schedule:
                    msg += 'and it will be closed after 2 hours.'
                    self.event_after_hours[win_num] = Clock.schedule_once(
                        partial(self.on_btn, 'window{}'.format(win_num), 'close'), 120 * 60)
                    self.is_aft_hr = True
                    # show_info_snackbar("Opened Window{}, and it will be closed after 2 hours.".format(win_num))
                else:
                    msg += 'and it won\'t be closed as the Time Schedule is disabled.'
                logger.info(msg)
        else:
            if _check_normal_time(win_num):
                if is_window_running:
                    self.on_btn('window{}'.format(win_num), 'stop')
                else:
                    self.on_btn('window{}'.format(win_num), 'close')
                    self.cancel_after_hours(win_num)
                self.is_aft_hr = False
            else:
                if is_window_running:
                    self.on_btn('window{}'.format(win_num), 'stop')
                else:
                    self.on_btn('window{}'.format(win_num), 'close')
                    self.cancel_after_hours(win_num)
                    self.is_aft_hr = False

    def cancel_after_hours(self, win_num):
        logger.info('Canceling `AfterHours` of window{}'.format(win_num))
        try:
            if self.event_after_hours[win_num]:
                self.event_after_hours[win_num].cancel()
        except Exception as e:
            logger.info('Failed to cancel AfterHours - {}'.format(e))
        self.event_after_hours[win_num] = None
        self.is_aft_hr = False

    def on_rain_event(self, event):
        if event:
            # When rain starts
            if not self.status['rain']['state']:
                logger.info("Attention! It's raining now!")
                self.close_all_windows()
        else:
            # When rain is just stopped
            if self.status['rain']['state']:
                self.status['rain']['time'] = time.time()
                logger.info('Rain is stopped')
            else:
                last_time = self.status['rain']['time']
                if last_time != -1:
                    delay = int(self.get_param_from_xml('RAIN_DELAY')) * 60
                    if delay == 0:
                        return False
                    if time.time() - last_time > delay:
                        logger.info('Rain delay starts...')
                        self.status['rain']['time'] = -1
                        self.restore_all_windows()

        # Update rain status
        self.status['rain']['state'] = event

    def on_wind_speed_event(self, speed):
        # Update GUI widgets
        self.screens['menu'].ids['lb_wind_speed'].text = '{} KPH'.format(round(speed, 2))
        self.screens['wind'].ids['lb_wind_speed'].text = 'WIND SPEED - {} KPH'.format(round(speed, 2))

        # check with threshold
        event = speed > float(self.get_param_from_xml('WIND_SPEED_TRIP'))
        if event:
            # When wind starts
            if not self.status['wind']['state']:
                logger.info('Attention! Strong wind!')
                self.close_all_windows()
        else:
            # When wind is just stopped
            if self.status['wind']['state']:
                self.status['wind']['time'] = time.time()
                logger.info('Safe wind...')
            else:
                last_time = self.status['wind']['time']
                if last_time != -1:
                    if time.time() - last_time > int(self.get_param_from_xml('WIND_DELAY')) * 60:
                        logger.info("Wind delay timer starts...")
                        self.status['wind']['time'] = -1
                        self.restore_all_windows()

        # Update rain status
        self.status['wind']['state'] = event

    def restore_all_windows(self):
        """
        Restore previous status of each window
        :return:
        """
        for device in ['window{}'.format(i) for i in range(1, WINDOW_CNT + 1)]:
            if self.last_cmd[device] == 'w_open':
                another_relay = 'w_close'
            else:
                another_relay = 'w_open'
            self.status[device][another_relay]['cnt'] = 0
            time.sleep(1)
            try:
                self.status[device][self.last_cmd[device]]['cnt'] = 1
            except KeyError:
                pass

    def close_all_windows(self):
        """
        Close all windows
        :return:
        """
        logger.info('Closing all windows...')
        # Stop all window relays
        for device in ['window{}'.format(i) for i in range(1, WINDOW_CNT + 1) if self.window_status[i-1]]:
            self.status[device]['w_open']['cnt'] = 0
        # Start window_close relays
        for device in ['window{}'.format(i) for i in range(1, WINDOW_CNT + 1) if self.window_status[i-1]]:
            self.on_btn(device, 'close')
        for i in [i for i in range(1, WINDOW_CNT + 1) if self.window_status[i-1]]:
            self.cancel_after_hours(i)

    def open_all_windows(self):
        logger.info('Opening all windows...')
        # Stop all window relays
        for device in ['window{}'.format(i) for i in range(1, WINDOW_CNT + 1) if self.window_status[i - 1]]:
            self.status[device]['w_close']['cnt'] = 0
        # Start window_open relays
        for device in ['window{}'.format(i) for i in range(1, WINDOW_CNT + 1) if self.window_status[i - 1]]:
            self.on_btn(device, 'open')

    def close_after_fire(self):
        logger.info('Schedule state: {}'.format(self.is_running_schedule))
        for n in [i for i in range(1, WINDOW_CNT + 1) if self.window_status[i - 1]]:
            if not self.is_running_schedule[n]:
                logger.info('Closing window {} after fire...'.format(n))
                self.on_btn('window{}'.format(n), 'close')

    def on_event_from_openhab(self, topic, command):
        """
        This function is called when a window(open/close) is turned on from the openhab web app.
        :param command: "ON" or "OFF"
        :param topic: "window1/w_open/set"
        :return:
        """
        if EN_OPENHAB:
            logger.info('Received a command from OPENHAB - `{}`/`{}`'.format(topic, command))
            [device, window, _] = topic.split('/')
            if command == 'ON':
                action = window.split('_')[1]
                self.on_btn(device, action)
            else:
                self.status[device][window]['cnt'] = 0

    def start_relay_on(self, device, window):
        """
        Start process of turning relay on...
        :param device:
        :param window:
        :return:
        """
        # start relay
        self.status[device][window]['cnt'] = 1
        if window == 'w_open':
            txt = 'Opening {}... '.format(device)
        else:
            txt = 'Closing {}...'.format(device)
        logger.info(txt)

        # Save last relay command to restore when it is safe.
        self.last_cmd[device] = window

        # popup = Popup(title='HVB-1', title_size=22, size_hint=(.6, .4),
        #               content=Label(text=txt, font_size=20))
        # popup.open()
        # Clock.schedule_once(partial(self.remove_popup, popup), 5)

    def execute_relay(self):
        """
        Executing routine of all windows
        This function is called every second
        :return:
        """
        for device in ['window{}'.format(i) for i in range(1, WINDOW_CNT + 1) if self.window_status[i - 1]]:
            for window in self.status[device].keys():
                cnt = self.status[device][window]['cnt']
                if cnt == 0:
                    if self.status[device][window]['state']:
                        client.publish(topic=device + '/' + window + '/set', payload='OFF')
                    self.screens['menu'].ids['btn_' + device + '_' + window].background_color = [1, 1, 1, 1]
                elif 0 < cnt < self.operation_time:
                    logger.debug('Relay is on, device: {}, window: {}, counter: {}'.format(device, window, cnt))
                    self.status[device][window]['cnt'] += 1
                    if self.status[device][window]['state'] is False:
                        client.publish(topic=device + '/' + window + '/set', payload='ON')
                        self.screens['menu'].ids['btn_' + device + '_' + window].background_color = [0, 1, 0, 1]
                else:
                    self.status[device][window]['cnt'] = 0
                    client.publish(topic=device + '/' + window + '/set', payload='OFF')
                    self.screens['menu'].ids['btn_' + device + '_' + window].background_color = [1, 1, 1, 1]

            if self.status[device]['w_open']['cnt'] > 0:
                st = 'Opening... {}'.format(self.status[device]['w_open']['cnt'])
            elif self.status[device]['w_close']['cnt'] > 0:
                st = 'Closing... {}'.format(self.status[device]['w_close']['cnt'])
                self.screens['menu'].ids['lb_state_' + device].text = st
            else:
                st = 'Open' if self.last_cmd[device] == 'w_open' else 'Close'
            self.screens['menu'].ids['lb_state_' + device].text = st
            client.publish(topic=device + '/cur_state', payload=st)

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        if dest_screen == 'settings':
            self.update_settings()
        elif dest_screen == 'system_settings':
            Clock.schedule_once(lambda dt: self._init_system_settings())
        elif dest_screen == 'menu':
            self.update_window_status()
        try:
            sm = self.root.ids.sm
            sm.transition = SlideTransition()
            screen = self.screens[dest_screen]
            sm.switch_to(screen, direction=direction)
            self.current_title = screen.name
        except ScreenManagerException:
            pass

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob(cur_dir + "screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        available_screens.append('schedule')

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        self.screens['schedule'] = ScheduleScreen()
        return True

    def on_btn(self, device, action, *args):
        """
        Callback function when OPEN/STOP/CLOSE button is pressed
        :param action: OPEN/CLOSE/STOP
        :param device: device name
        :return:
        """
        if action == 'open':
            # Disable OPEN when security closed, but not FIRE
            if self.last_security_closed_time > 0 > self.last_fire_opened_time:
                logger.info('Cannot open {}, SECURITY_CLOSE!'.format(device))
                return
        elif action == 'close':
            if self.last_fire_opened_time > 0:
                logger.info('Cannot close {}, FIRE!'.format(device))
                return
        else:
            if self.last_fire_opened_time > 0:
                logger.info('Cannot stop {}, FIRE!'.format(device))
                return
            elif self.last_security_closed_time > 0:
                logger.info('Cannot stop {}, SECURITY!'.format(device))
                return

        if action == 'stop':
            for window in self.status[device].keys():
                if self.status[device][window]['cnt'] > 1:
                    self.status[device][window]['cnt'] = 0
                    popup = Popup(title='Zuccoli', title_size=22, size_hint=(.6, .4),
                                  content=Label(text='Stopping {} of {}...'.format(window, device), font_size=20))
                    # popup.open()
                    logger.info('Stopping {} of {}...'.format(window, device))
                    Clock.schedule_once(lambda dt: self.remove_popup(popup), 5)
        else:
            window = 'w_' + action
            another_window = 'w_open' if window == 'w_close' else 'w_close'
            if self.status[device][window]['cnt'] > 0:
                logger.warning('Error, {} of {} is already running...'.format(window, device))
            elif self.status[device][another_window]['cnt'] > 0:
                # Popup(title='HVB-1', title_size=22, size_hint=(.6, .4),
                #       content=Label(text='Another relay on this device is running...', font_size=20)).open()
                self.status[device][another_window]['cnt'] = 0
                self.cnt = 0
                Clock.schedule_once(partial(self.check_another_window, device, window), 1)
            else:
                self.cnt = 0
                Clock.schedule_once(partial(self.check_another_window, device, window), 1)

    def check_another_window(self, *args):
        device = args[0]
        window = args[1]
        # Wait until another window is turned off...
        another_window = 'w_open' if window == 'w_close' else 'w_close'
        self.status[device][another_window]['cnt'] = 0
        if self.status[device][another_window]['state']:
            if self.cnt > 10:
                error_text = 'Failed to turn {} off of {}'.format(another_window, device)
                Popup(title='Zuccoli', title_size=22, size_hint=(.6, .4),
                      content=Label(text=error_text, font_size=20))
                logger.info(error_text)
                return False
            else:
                self.cnt += 1
                logger.info('Waiting for another window to be turned off.')
                Clock.schedule_once(partial(self.check_another_window, device, window), 1)
        else:
            # After checking rule, let us turn the corresponding relay on!
            if self.status[device][window]['cnt'] == 0:
                self.start_relay_on(device, window)
            else:
                logger.warning('Can\'t start {} of {}, already running'.format(device, window))

    @staticmethod
    def remove_popup(popup):
        """
        Dismiss given popup after a certain time duration.
        :param popup:
        :return:
        """
        popup.dismiss()

    def draw_wind_dir(self, direction):
        """
        Draw a round compass indicating wind direction.
        :param direction:
        :return:
        """
        # angle = 2 * pi / 60 * datetime.datetime.now().second
        center = [400, 300]
        # Not sure why I have to multiply 6 with this
        # But measured values are ---     North: 1.05, East: 15, South: 30, West: 45
        angle = 2 * pi / 360 * (direction - 1.0) * 6
        panel = self.screens['wind'].ids['panel']
        panel.canvas.clear()
        with panel.canvas:
            Color(0.2, 0.5, 0.2)
            offset_x = 0.15 * panel.width * sin(angle)
            offset_y = 0.15 * panel.width * cos(angle)
            Line(points=[center[0] - offset_x, center[1] - offset_y, center[0] + offset_x, center[1] + offset_y],
                 width=2)
            arrow_1_x = 20 * cos(pi/2 - angle - pi/6)
            arrow_1_y = 20 * sin(pi / 2 - angle - pi / 6)
            Line(points=[center[0] + offset_x, center[1] + offset_y, center[0] + offset_x - arrow_1_x,
                         center[1] + offset_y - arrow_1_y], width=2)
            arrow_2_x = 20 * cos(pi / 2 - angle + pi / 6)
            arrow_2_y = 20 * sin(pi / 2 - angle + pi / 6)
            Line(points=[center[0] + offset_x, center[1] + offset_y, center[0] + offset_x - arrow_2_x,
                         center[1] + offset_y - arrow_2_y], width=2)

        angle = (direction - 1.0) * 6
        dir_text = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'][int((angle - 22.5) / 45)]
        client.publish('window/wind_dir', dir_text)

    def btn_settings(self, tag_name, val):
        """
        Update settings value
        :param tag_name:
        :param val:
        :return:
        """
        self.wind_trip = int(self.get_param_from_xml('WIND_SPEED_TRIP'))
        self.wind_delay = int(self.get_param_from_xml('WIND_DELAY'))
        self.rain_delay = int(self.get_param_from_xml('RAIN_DELAY'))
        if tag_name == 'WIND_SPEED_TRIP':
            self.wind_trip += val
            if self.wind_trip < 0:
                self.wind_trip = 0
            self.set_param_to_xml(tag_name, self.wind_trip)
        elif tag_name == 'WIND_DELAY':
            self.wind_delay += val
            if self.wind_delay < 0:
                self.wind_delay = 0
            self.set_param_to_xml(tag_name, self.wind_delay)
        elif tag_name == 'RAIN_DELAY':
            self.rain_delay += val
            if self.rain_delay < 0:
                self.rain_delay = 0
            self.set_param_to_xml(tag_name, self.rain_delay)
        elif tag_name == 'CLOSE_AFTER_FIRE':
            self.sec_close_after_fire += val
            if self.sec_close_after_fire < 0:
                self.sec_close_after_fire = 0
            self.set_param_to_xml(tag_name, self.sec_close_after_fire)
        elif tag_name == 'WINDOW_OPERATION_TIME':
            scr = self.screens['settings']
            new_val = int(scr.ids['lb_window_opt_time'].text) + val
            self.set_param_to_xml(tag_name, new_val)
            self.operation_time = new_val
        self.update_settings()

    def update_settings(self):
        """
        Update settings widgets with current values
        :return:
        """
        scr = self.screens['settings']
        scr.ids['lb_wind_speed_trip'].text = str(self.wind_trip)
        scr.ids['lb_wind_delay'].text = str(self.wind_delay)
        scr.ids['lb_rain_delay'].text = str(self.rain_delay)
        scr.ids['lb_close_after_fire'].text = str(self.sec_close_after_fire)
        scr.ids['lb_window_opt_time'].text = str(self.operation_time)
        scr.ids['spinner_security'].text = self.security_num
        scr.ids['spinner_inner_temp'].text = self.inner_num
        scr.ids['spinner_outer_temp'].text = self.outer_num
        scr.ids['spinner_rain'].text = self.rain_num
        scr.ids['spinner_wind'].text = self.wind_num
        scr.ids['spinner_air_cond'].text = self.get_param_from_xml("AIR_COND_ACTION")

    def on_spinner_settings_changed(self, param_name, *args):
        self.set_param_to_xml(param_name, args[0].text)
        if param_name == 'INNER_TEMP_WINDOW_NUM':
            self.inner_num = args[0].text
        elif param_name == 'OUTER_TEMP_WINDOW_NUM':
            self.outer_num = args[0].text
        elif param_name == 'RAIN_WINDOW_NUM':
            self.rain_num = args[0].text
        elif param_name == 'WIND_WINDOW_NUM':
            self.wind_num = args[0].text
        elif param_name == 'SECURITY_WINDOW_NUM':
            self.security_num = args[0].text

    def on_check_window(self, *args):
        pass

    def touch_wind(self, *args):
        """
        Check current touched position is for the wind widget
        :return:
        """
        if self.screens['menu'].ids['ly_wind'].collide_point(*args[1].pos) and self.en_wind:
            self.go_screen('wind', 'down')

    def btn_back(self):
        """
        When user presses "<<" button in the pin input screen
        :return:
        """
        pin = self.screens['pin'].ids['txt_pin'].text
        if len(pin) > 0:
            self.screens['pin'].ids['txt_pin'].text = pin[:-1]

    def btn_pin(self):
        """
        When the user presses 'ENTER' button in the pin screen
        :return:
        """
        pin = self.screens['pin'].ids['txt_pin'].text
        if len(pin) == 0:
            Popup(title='Notification', title_size=25, size_hint=(.6, .4),
                  content=Label(text='Please input PIN CODE.', font_size=25)).open()
            return False

        if self.screens['pin'].ids['button_pin'].text == 'REGISTER':
            self.set_param_to_xml('PIN', pin)
            Popup(title='Notification', title_size=25, size_hint=(.6, .4),
                  content=Label(text='PIN CODE is updated.', font_size=25)).open()
            self.screens['pin'].ids['button_pin'].text = 'ENTER'
            self.screens['pin'].ids['txt_pin'].text = ''
            self.go_screen('menu', 'up')
        else:
            if pin == self.get_param_from_xml('MASTER_PIN'):
                self.screens['pin'].ids['txt_pin'].text = ''
                Popup(title='Notification', title_size=25, size_hint=(.6, .4),
                      content=Label(text='Please register your PIN CODE.', font_size=25)).open()
                self.screens['pin'].ids['button_pin'].text = 'REGISTER'
                return True
            else:
                self.screens['pin'].ids['txt_pin'].text = ''
                if pin != self.get_param_from_xml('PIN'):
                    Popup(title='Error', title_size=25, size_hint=(.6, .4),
                          content=Label(text='Invalid PIN CODE.', font_size=25)).open()
                else:
                    self.go_screen('settings', 'up')

    def set_param_to_xml(self, tag_name, new_val):
        if not isinstance(new_val, basestring):
            new_val = str(new_val)
        et = xml.etree.ElementTree.parse(self.conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name, xml_declaration=True, encoding='utf-8', method="xml")
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        try:
            root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
            tmp = None
            for child_of_root in root:
                if child_of_root.tag == param:
                    tmp = child_of_root.text
                    break
            return tmp
        except Exception as e:
            logger.info('Failed to get param from xml - {}'.format(e))

    def count_down(self, *args):
        global counter
        if counter > 0:
            # self.turn_bright(255)
            # self.root.ids.img_logo_full.opacity = 0
            counter -= 1
        else:
            if counter == 0:
                self.screen_saver.open()
                counter = -1
            # self.root.ids.img_logo_full.opacity = 1
            # self.turn_bright(100)
        # print counter

    def on_sensor_setting(self, widget, sensor):
        new_val = 'enable' if widget.active else 'disable'
        logger.info('Setting {} to be {}d'.format(sensor, new_val))
        self.set_param_to_xml(sensor.upper(), new_val)
        if sensor == 'rain':
            self.en_rain = widget.active
            self.screens['menu'].ids['lb_rain'].opacity = 1 if self.en_rain else 0
        elif sensor == 'wind':
            self.en_wind = widget.active
            self.screens['menu'].ids['ly_wind'].opacity = 1 if self.en_wind else 0
        elif sensor == 'time_schedule':
            self.en_time_schedule = widget.active
        else:
            self.en_temp = widget.active
            self.screens['menu'].ids['bl_temp'].opacity = 1 if self.en_temp else 0
            # if not self.en_temp:
            #     self.screens['menu'].ids['lb_temp_in'].text = '[color=A0A0A0]disabled[/color]'
            # else:
            #     self.screens['menu'].ids['lb_temp_in'].text = ''

    def update_sensor_settings(self):
        for sensor in ['rain', 'wind', 'temp', 'time_schedule']:
            val = self.get_param_from_xml(sensor.upper())
            self.screens['settings'].ids['chk_{}'.format(sensor)].active = (val == 'enable')
            setattr(self, 'en_{}'.format(sensor), val == 'enable')
            if sensor == 'rain':
                self.screens['menu'].ids['lb_rain'].opacity = 1 if self.en_rain else 0
            elif sensor == 'wind':
                self.screens['menu'].ids['ly_wind'].opacity = 1 if self.en_wind else 0
            elif sensor == 'time_schedule':
                pass
            else:
                self.screens['menu'].ids['bl_temp'].opacity = 1 if self.en_temp else 0
                # if not self.en_temp:
                #     self.screens['menu'].ids['lb_temp_in'].text = '[color=A0A0A0]disabled[/color]'
            # if not self.en_wind:
            #     self.screens['menu'].ids.lb_wind_speed.text = 'disabled'
            #     self.screens['menu'].ids.lb_wind_speed.color = [.6, .6, .6, 1]
            # else:
            #     self.screens['menu'].ids.lb_wind_speed.color = [1, 1, 1, 1]

    @staticmethod
    def open_window_dialog():
        WindowPopup().open()

    def apply_window_names(self, popup):
        popup.dismiss()
        for i in range(WINDOW_CNT):
            new_val = popup.ids['txt_window{}'.format(i)].text
            self.set_param_to_xml('WINDOW{}'.format(i), new_val)
            self.screens['menu'].ids['lb_window_name_{}'.format(i)].text = new_val
            new_val = 'ON' if popup.ids['chk_window{}'.format(i)].active else 'OFF'
            self.set_param_to_xml('WINDOW_ACTIVE_{}'.format(i), new_val)
            new_val = 'ON' if popup.ids['chk_fire_{}'.format(i)].active else 'OFF'
            self.set_param_to_xml('FIRE_{}'.format(i), new_val)
            self.fire_enabled[i] = new_val == 'ON'
            new_val = 'ON' if popup.ids['chk_air_{}'.format(i)].active else 'OFF'
            self.set_param_to_xml('AIR_{}'.format(i), new_val)
            self.air_enabled[i] = new_val == 'ON'
        self.update_air_cond_label()
        self._update_openhab_sitemap()

    @staticmethod
    def turn_bright(n, *args):
        """
        Adjust brightness of touch screen
        :param n: brightness value, range 0 ~ 255
        :param args:
        :return:
        """
        if is_rpi():
            pipe = os.popen('cat /sys/class/backlight/rpi_backlight/brightness')
            data = pipe.read().strip()
            pipe.close()
            if data != str(n):
                logger.info('Changing brightness to {}'.format(n))
                os.system('sudo echo {} > /sys/class/backlight/rpi_backlight/brightness'.format(n))

    def check_schedule(self, *args):
        if datetime.datetime.now().second != 0:
            return
        with open('config.xml', 'r') as fd:
            _xml = xmltodict.parse(fd.read())
        cur_day = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'][
            datetime.datetime.today().weekday()]
        for i in range(1, WINDOW_CNT + 1):
            schedule = _xml['CONFIG']['SCHEDULE']['WINDOW{}'.format(i)]
            if schedule[cur_day]['ACTIVE'] == 'ON':
                if schedule[cur_day]['START'] == datetime.datetime.now().strftime('%H:%M'):
                    logger.info(
                        '**** Opening Window {} by schedule - {}'.format(i, datetime.datetime.now().strftime('%H:%M')))
                    self.on_btn('window{}'.format(i), 'open')
                    self.is_running_schedule[i] = True
                    self.cancel_after_hours(i)
                elif schedule[cur_day]['STOP'] == datetime.datetime.now().strftime('%H:%M'):
                    logger.info(
                        '**** Closing Window {} by schedule - {}'.format(i, datetime.datetime.now().strftime('%H:%M')))
                    self.on_btn('window{}'.format(i), 'close')
                    self.is_running_schedule[i] = False
                    self.cancel_after_hours(i)

    def check_automatic_time_schedule(self):
        if datetime.datetime.now().second != 0:
            return
        with open('config.xml', 'r') as fd:
            _xml = xmltodict.parse(fd.read())
        sch = _xml['CONFIG']['AUTOMATIC_TIME_SCHEDULE']
        if sch['TYPE'] == 'GLOBAL':
            if sch['GLOBAL_EN_OPEN'] == 'ON':
                if sch['GLOBAL_OPEN'] == datetime.datetime.now().strftime('%H:%M'):
                    logger.info("**** Opening ALL windows by GLOBAL schedule")
                    for i in range(1, WINDOW_CNT + 1):
                        self.on_btn('window{}'.format(i), 'open')
                        self.is_running_schedule[i] = True
                        self.cancel_after_hours(i)
            if sch['GLOBAL_EN_CLOSE'] == 'ON':
                if sch['GLOBAL_CLOSE'] == datetime.datetime.now().strftime('%H:%M'):
                    logger.info('**** Closing ALL windows by GLOBAL schedule')
                    for i in [i for i in range(1, WINDOW_CNT + 1) if self.window_status[i - 1]]:
                        self.on_btn('window{}'.format(i), 'close')
                        self.is_running_schedule[i] = False
                        self.cancel_after_hours(i)
        else:
            for i in range(1, WINDOW_CNT + 1):
                s = sch['WINDOW{}'.format(i)]
                if s['ACTIVE'] == 'ON':
                    if s['EN_OPEN'] == 'ON':
                        if s['OPEN'] == datetime.datetime.now().strftime('%H:%M'):
                            logger.info('**** Opening Window {} by schedule - {}'.format(i, s['OPEN']))
                            self.on_btn('window{}'.format(i), 'open')
                            self.is_running_schedule[i] = True
                            self.cancel_after_hours(i)
                    if s['EN_CLOSE'] == 'ON':
                        if s['CLOSE'] == datetime.datetime.now().strftime('%H:%M'):
                            logger.info('**** Closing Window {} by schedule - {}'.format(i, s['CLOSE']))
                            self.on_btn('window{}'.format(i), 'close')
                            self.is_running_schedule[i] = False
                            self.cancel_after_hours(i)

    def show_time(self, *args):
        _time = datetime.datetime.now()
        str_time = _time.strftime("%B %d, %Y  %H:%M:%S")
        self.screens['menu'].ids.lb_time.text = '{}, {}'.format(calendar.day_name[_time.weekday()][:3], str_time)

    def update_window_status(self):
        scr = self.screens['menu']
        for i in range(WINDOW_CNT):
            scr.ids['lb_window_name_{}'.format(i)].text = str(self.get_param_from_xml('WINDOW{}'.format(i)))
            self.window_status[i] = (self.get_param_from_xml('WINDOW_ACTIVE_{}'.format(i)) == 'ON')
            box = scr.ids['box_{}'.format(i)]
            box.disabled = not self.window_status[i]
            box.opacity = 1 if self.window_status[i] else 0

    def on_stop(self):
        logger.info('======== Why are you closing me? =======')
        super(MainApp, self).on_stop()

    def btn_system_settings_apply(self):
        old_ssid = self.get_param_from_xml('SSID')
        ssid = self.screens['system_settings'].ids.txt_ssid.text.strip()
        reboot_required = False
        if ssid and old_ssid != ssid:
            if net_util.change_ssid(old_ssid, ssid):
                self.set_param_to_xml('SSID', ssid)
                logger.info('WiFi SSID: Successfully changed to `{}`'.format(ssid))
                if is_rpi():
                    reboot_required = True
        old_interface = self.get_param_from_xml('WIFI_INTERFACE')
        cur_interface = 'builtin' if self.screens['system_settings'].ids.chk_wifi_builtin.active else 'usb'
        if old_interface != cur_interface:
            self.set_param_to_xml('WIFI_INTERFACE', cur_interface)
            net_util.change_wifi_interface(cur_interface)
            logger.info('WiFi interface: Changed to {}'.format(cur_interface))
            if is_rpi():
                reboot_required = True

        str_date = self.date_picker.text
        str_time = self.time_picker.text
        local_dt = datetime.datetime.strptime('{} {}'.format(str_date, str_time), "%Y-%d-%m %H:%M")
        update_system_datetime(local_dt)

        if reboot_required:
            os.system('reboot')
        else:
            self.go_screen('settings', 'right')

    def _init_system_settings(self):
        scr = self.screens['system_settings']
        scr.ids.txt_ssid.text = self.get_param_from_xml('SSID')
        scr.ids.chk_wifi_builtin.active = self.get_param_from_xml('WIFI_INTERFACE') == 'builtin'
        scr.ids.chk_wifi_usb.active = self.get_param_from_xml('WIFI_INTERFACE') == 'usb'
        container = scr.ids.container
        container.clear_widgets()
        container.add_widget(self.date_picker)
        container.add_widget(Label(text="_", size_hint=(None, None), width=20, pos_hint={'center_y': .8}))
        container.add_widget(self.time_picker)

    def check_activity(self, *args):
        for i in range(1, WINDOW_CNT + 1):
            if self.window_status[i - 1]:
                if time.time() - self.last_time[i] > 60:
                    self.screens['menu'].ids['lb_window_name_{}'.format(i - 1)].color = (1, 0, 0, 1)
                else:
                    self.screens['menu'].ids['lb_window_name_{}'.format(i - 1)].color = (1, 1, 1, 1)

    def show_after_hours_popup(self, popup):
        popup.dismiss()
        del popup
        gc.collect()
        p = AfterHoursPopup()
        p.bind(on_dismiss=self.on_after_hours_popup_closed)
        p.open()

    def show_time_schedule_popup(self, popup):
        popup.dismiss()
        del popup
        gc.collect()
        p = TimeSchedulePopup()
        p.bind(on_dismiss=self.on_after_hours_popup_closed)
        p.open()

    def on_after_hours_popup_closed(self, *args):
        self.show_after_hours_label()

    def show_after_hours_label(self):
        self.screens['menu'].ids['lb_after_hours'].opacity = 1 if any(
            self.get_param_from_xml('AFTER_HOURS_{}'.format(i)) != 'OFF' for i in range(1, WINDOW_CNT + 1)) else 0

    def update_air_cond_label(self):
        self.screens['menu'].ids['lb_air_cond'].opacity = 1 \
            if any(self.get_param_from_xml('AIR_{}'.format(i)) == 'ON' for i in range(WINDOW_CNT)) else 0

    def _update_openhab_sitemap(self):
        if EN_OPENHAB:
            conf_file = os.path.join(cur_dir, os.pardir, 'openhab', 'configurations', 'sitemaps', 'default.sitemap')
            conf = open(conf_file).read().splitlines()
            i = 0
            while i < len(conf):
                for j in range(WINDOW_CNT):
                    _keyword = 'WINDOW_NAME_{}_HERE'.format(j)
                    if _keyword in conf[i]:
                        if self.get_param_from_xml('WINDOW_ACTIVE_{}'.format(j)) != 'ON':
                            conf = conf[:i] + conf[i + 5:]
                        else:
                            conf[i] = conf[i].replace(_keyword, self.get_param_from_xml('WINDOW{}'.format(j)))
                i += 1
            if is_rpi():
                open('/opt/openhab/configurations/sitemaps/default.sitemap', 'w').write('\n'.join(conf))


if __name__ == '__main__':

    app = MainApp()
    app.run()
