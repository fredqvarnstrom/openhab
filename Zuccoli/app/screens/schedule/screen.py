import os
from functools import partial

import datetime
from kivy.core.window import Window
from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, BooleanProperty, DictProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen
import xmltodict

from screens.schedule.time_picker.time_picker import CircularTimePicker

Builder.load_file(os.path.join(os.path.dirname(__file__), 'schedule.kv'))


class ScheduleItem(BoxLayout):

    day = StringProperty('')
    start_time = ObjectProperty(None)
    stop_time = ObjectProperty(None)
    active = BooleanProperty(True)

    def __init__(self, **kwargs):
        super(ScheduleItem, self).__init__(**kwargs)
        self.register_event_type('on_touch_start')
        self.register_event_type('on_touch_stop')
        Clock.schedule_once(self.bind_touch)

    def bind_touch(self, *args):
        self.ids.tp_start.bind(focus=partial(self.on_focus, 'start_time'))
        self.ids.tp_stop.bind(focus=partial(self.on_focus, 'stop_time'))

    def update_times(self, *args):
        self.ids.tp_start.text = self.start_time
        self.ids.tp_stop.text = self.stop_time
        self.ids.chk.active = self.active

    def on_focus(self, inst, *args):
        if args[1]:
            Window.release_all_keyboards()
            if inst == 'start_time':
                self.dispatch('on_touch_start')
            elif inst == 'stop_time':
                self.dispatch('on_touch_stop')

    def on_touch_start(self):
        pass

    def on_touch_stop(self):
        pass


class ScheduleScreen(Screen):

    win_num = NumericProperty(1)
    popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ScheduleScreen, self).__init__(**kwargs)

    def update_windows(self, *args):
        with open('config.xml') as fd:
            xml = xmltodict.parse(fd.read())
        schedule = xml['CONFIG']['SCHEDULE']['WINDOW{}'.format(self.win_num)]
        for day in ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY']:
            tp = self.ids['tp_{}'.format(day)]
            tp.active = True if schedule[day]['ACTIVE'] == 'ON' else False
            tp.start_time = schedule[day]['START']
            tp.stop_time = schedule[day]['STOP']
            tp.update_times()

    def on_touch(self, widget, s_type):
        # print 'day: {}, type: {}'.format(widget.day, s_type)
        if s_type == 'start':
            cur_wid = widget.ids.tp_start
        else:
            cur_wid = widget.ids.tp_stop
        cur_time = datetime.datetime.strptime(cur_wid.text, '%H:%M').time()
        self.popup = Popup(content=CircularTimePicker(time=cur_time), size_hint=(.5, .8),
                           on_dismiss=partial(self.update_value, cur_wid), title="Pick Time")
        self.popup.open()

    def on_enter(self, *args):
        self.win_num = App.get_running_app().cur_window
        Clock.schedule_once(self.update_windows)

    def update_value(self, cur_wid, *args):
        cur_time = self.popup.content.time
        str_time = "%.2d:%.2d" % (cur_time.hour, cur_time.minute)
        cur_wid.text = str_time

    def on_apply(self):
        with open('config.xml', 'r') as fd:
            xml = xmltodict.parse(fd.read())

        schedule = {}
        for day in ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY']:
            wid = self.ids['tp_{}'.format(day)]
            schedule[day] = {
                'ACTIVE': 'ON' if wid.ids.chk.active else 'OFF',
                'START': wid.ids.tp_start.text,
                'STOP': wid.ids.tp_stop.text
            }

        with open('config.xml', 'w') as fd:
            xml['CONFIG']['SCHEDULE']['WINDOW{}'.format(self.win_num)] = schedule
            fd.write(xmltodict.unparse(xml))

        App.get_running_app().go_screen('settings', 'right')
