import datetime
import os
import subprocess
import time


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    try:
        if len(os.popen("ps -aef | grep -i '%s' "
                        "| grep -v 'grep' | awk '{ print $3 }'" % proc_name).read().strip().splitlines()) > 0:
            return True
    except Exception as e:
        print('Failed to get status of the process({}) - {}'.format(proc_name, e))
    return False


PROC_NAME = "java -Dosgi.clean=true -Declipse.ignoreApp=true"


def main():
    last_check_time = 0
    while True:
        if not check_running_proc(PROC_NAME):
            if time.time() - last_check_time > 60:
                print("{}:: OPENHAB is not running... Repairing now...".format(datetime.datetime.now()))
                subprocess.Popen('sudo screen -S oh -X stuff "bash start.sh\r"', shell=True)
                last_check_time = time.time()
        time.sleep(1)


if __name__ == '__main__':
    main()
