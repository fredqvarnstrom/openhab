import datetime

from kivy.logger import Logger

PREFIX = 'Zuccoli'


def debug(msg):
    Logger.debug('{} - {}: {}'.format(PREFIX, datetime.datetime.now(), msg))


def info(msg):
    Logger.info('{} - {}: {}'.format(PREFIX, datetime.datetime.now(), msg))


def warning(msg):
    Logger.warning('{} - {}: {}'.format(PREFIX, datetime.datetime.now(), msg))


def error(msg):
    Logger.error('{} - {}: {}'.format(PREFIX, datetime.datetime.now(), msg))


def exception(e):
    Logger.exception('{} - {}: {}'.format(PREFIX, datetime.datetime.now(), e))
