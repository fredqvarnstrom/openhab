from kivy.lang import Builder
from kivy.properties import ListProperty, NumericProperty, OptionProperty
from kivy.uix.relativelayout import RelativeLayout


Builder.load_file('widgets/kv/separator.kv')


class Separator(RelativeLayout):
    color = ListProperty([0.8, 0.8, 0.8, 0.8])
    s_width = NumericProperty(1)
    orientation = OptionProperty('vertical', options=['vertical', 'horizontal'])
    animate_scale = False
