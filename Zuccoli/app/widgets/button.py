import os

from assets.styles import defaultstyle
from kivy.lang import Builder
from kivy.properties import OptionProperty, NumericProperty, StringProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.vector import Vector

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'button.kv'))


class ZuccoliButton(Button):
    button_type = OptionProperty('blue', options=['blue', 'white'])
    font_size = NumericProperty(17)

    def __init__(self, **kwargs):
        super(ZuccoliButton, self).__init__(**kwargs)
        self.set_default_color()

    def set_white_color(self):
        self.color = defaultstyle.COLOR_1
        self.background_normal = 'assets/images/buttons/white_button.png'
        self.background_down = 'assets/images/buttons/white_button_pressed.png'

    def set_default_color(self):
        self.color = (1, 1, 1, 1)
        self.background_normal = 'assets/images/buttons/blue_button.png'
        self.background_down = 'assets/images/buttons/blue_button_pressed.png'

    def on_button_type(self, obj, val):
        if val == 'blue':
            obj.set_default_color()
        else:
            self.set_white_color()


class CircularButton(ButtonBehavior, Widget):

    text = StringProperty('')
    text_color = ListProperty([.1, .1, .1, 1])
    background_color = ListProperty([0.13, 0.59, 0.95, 1])

    def collide_point(self, x, y):
        return Vector(x, y).distance(self.center) <= self.width / 2
