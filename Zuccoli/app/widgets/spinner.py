# -*- coding: iso8859-15 -*-
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.spinner import Spinner
from widgets.button import ZuccoliButton


Builder.load_file('widgets/kv/spinner.kv')


class ZuccoliSpinnerOption(ZuccoliButton):
    pass


class ZuccoliSpinner(Spinner):

    option_cls = ObjectProperty(ZuccoliSpinnerOption)

    def __init__(self, **kwargs):
        super(ZuccoliSpinner, self).__init__(**kwargs)
        self.register_event_type('on_changed')

    def _on_dropdown_select(self, *args):
        super(ZuccoliSpinner, self)._on_dropdown_select(*args)
        self.dispatch('on_changed')

    def get_value(self):
        return self.text

    def set_value(self, val):
        self.text = val

    def on_changed(self):
        pass
