import datetime
import os
from functools import partial

import xmltodict
from kivy.app import App
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.properties import BooleanProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.modalview import ModalView
from kivy.uix.popup import Popup
from screens.schedule.time_picker.time_picker import CircularTimePicker
from settings import WINDOW_CNT

Builder.load_file(os.path.join(os.path.dirname(__file__), 'popups.kv'))


class WindowPopup(Popup):

    def __init__(self, **kwargs):
        super(WindowPopup, self).__init__(**kwargs)
        for i in range(WINDOW_CNT):
            _app = App.get_running_app()
            self.ids['txt_window{}'.format(i)].text = _app.get_param_from_xml('WINDOW{}'.format(i))
            active = (_app.get_param_from_xml('WINDOW_ACTIVE_{}'.format(i)) == 'ON')
            self.ids['chk_window{}'.format(i)].active = active
            active = (_app.get_param_from_xml('FIRE_{}'.format(i)) == 'ON')
            self.ids['chk_fire_{}'.format(i)].active = active
            active = (_app.get_param_from_xml('AIR_{}'.format(i)) == 'ON')
            self.ids['chk_air_{}'.format(i)].active = active

    def go_schedule(self, win_num):
        self.dismiss()
        App.get_running_app().cur_window = win_num
        App.get_running_app().go_screen('schedule', 'left')


class ScreenSaverPopup(ModalView):

    event_adv = None

    def on_open(self):
        if self.event_adv is None:
            self.event_adv = Clock.schedule_interval(self.show_adv, .02)

    def show_adv(self, *args):
        wid = self.ids.logo
        if wid.x < 700:
            wid.x += 3
        else:
            wid.x = 50

    def on_touch_down(self, touch):
        self.dismiss()

    def on_dismiss(self):
        super(ScreenSaverPopup, self).on_dismiss()
        if self.event_adv:
            self.event_adv.cancel()
            self.event_adv = None


class AfterHoursPopup(Popup):

    _app = None

    def on_open(self):
        self._app = App.get_running_app()
        for i in range(1, WINDOW_CNT + 1):
            self.ids['lb_window_{}'.format(i)].text = self._app.get_param_from_xml('WINDOW{}'.format(i - 1))
            state = self._app.get_param_from_xml('AFTER_HOURS_{}'.format(i))
            if state == 'LOCAL':
                self.ids['chk_local_{}'.format(i)].active = True
            else:
                self.ids['chk_global_{}'.format(i)].active = True
            self.ids['chk_window{}'.format(i)].active = False if state == 'OFF' else True

    def on_btn_apply(self):
        for i in range(1, WINDOW_CNT + 1):
            if not self.ids['chk_window{}'.format(i)].active:
                val = 'OFF'
            elif self.ids['chk_local_{}'.format(i)].active:
                val = 'LOCAL'
            else:
                val = 'GLOBAL'
            self._app.set_param_to_xml('AFTER_HOURS_{}'.format(i), val)
        self.dismiss()


class TimeScheduleItem(BoxLayout):

    enabled = BooleanProperty()
    window_name = StringProperty()
    open_time = StringProperty()
    close_time = StringProperty()
    en_open = BooleanProperty(True)
    en_close = BooleanProperty(True)

    def __init__(self, **kwargs):
        super(TimeScheduleItem, self).__init__(**kwargs)
        self.register_event_type('on_focused')
        Clock.schedule_once(lambda dt: self._bind_events())

    def _bind_events(self):
        self.ids.txt_open.bind(focus=self._on_text_touched)
        self.ids.txt_close.bind(focus=self._on_text_touched)

    def _on_text_touched(self, *args):
        self.dispatch('on_focused', *args)

    def on_focused(self, *args):
        pass


class TimeSchedulePopup(Popup):

    _app = None

    def __init__(self, **kwargs):
        super(TimeSchedulePopup, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self._bind_events())

    def _bind_events(self):
        self.ids.txt_global_open.bind(focus=self._show_global_time_picker)
        self.ids.txt_global_close.bind(focus=self._show_global_time_picker)
        for i in range(1, WINDOW_CNT + 1):
            self.ids['ts_item_{}'.format(i)].bind(on_focused=self._show_local_time_picker)

    def on_open(self):
        self._app = App.get_running_app()
        with open('config.xml', 'r') as fd:
            _xml = xmltodict.parse(fd.read())
        schedule = _xml['CONFIG']['AUTOMATIC_TIME_SCHEDULE']
        self.ids['chk_{}'.format(str(schedule['TYPE']).lower())].active = True
        self.ids.chk_global_open.active = schedule['GLOBAL_EN_OPEN'] == 'ON'
        self.ids.txt_global_open.text = schedule['GLOBAL_OPEN']
        self.ids.chk_global_close.active = schedule['GLOBAL_EN_CLOSE'] == 'ON'
        self.ids.txt_global_close.text = schedule['GLOBAL_CLOSE']
        for i in range(1, WINDOW_CNT + 1):
            item = self.ids['ts_item_{}'.format(i)]
            item.window_name = _xml['CONFIG']['WINDOW{}'.format(i - 1)]
            sch = schedule['WINDOW{}'.format(i)]
            item.enabled = sch['ACTIVE'] == 'ON'
            item.en_open = sch['EN_OPEN'] == 'ON'
            item.open_time = sch['OPEN']
            item.en_close = sch['EN_CLOSE'] == 'ON'
            item.close_time = sch['CLOSE']

    def _show_global_time_picker(self, inst, val):
        if val:
            self.show_time_picker(inst)

    def _show_local_time_picker(self, item, inst, val):
        if val:
            self.show_time_picker(inst)

    def show_time_picker(self, inst):
        if inst.text:
            cur_time = datetime.datetime.strptime(inst.text, '%H:%M').time()
        else:
            cur_time = datetime.datetime.now().time()
        popup = Popup(content=CircularTimePicker(time=cur_time), on_dismiss=partial(self._update_time_value, inst),
                      title="Pick Time", size_hint=(.5, .8))
        Window.release_all_keyboards()
        popup.open()

    @staticmethod
    def _update_time_value(inst, popup):
        cur_time = popup.content.time
        inst.text = "%.2d:%.2d" % (cur_time.hour, cur_time.minute)
        inst.focus = False

    def on_btn_apply(self):
        with open('config.xml', 'r') as fd:
            xml = xmltodict.parse(fd.read())

        schedule = {
            'TYPE': 'GLOBAL' if self.ids.chk_global.active else "LOCAL",
            'GLOBAL_EN_OPEN': 'ON' if self.ids.chk_global_open.active else 'OFF',
            'GLOBAL_OPEN': self.ids.txt_global_open.text,
            'GLOBAL_EN_CLOSE': 'ON' if self.ids.chk_global_close.active else 'OFF',
            'GLOBAL_CLOSE': self.ids.txt_global_close.text,
        }
        for i in range(1, WINDOW_CNT + 1):
            item = self.ids['ts_item_{}'.format(i)]
            schedule['WINDOW{}'.format(i)] = dict(
                ACTIVE='ON' if item.ids.chk.active else 'OFF',
                EN_OPEN='ON' if item.ids.chk_open.active else 'OFF',
                OPEN=item.ids.txt_open.text,
                EN_CLOSE='ON' if item.ids.chk_close.active else 'OFF',
                CLOSE=item.ids.txt_close.text
            )

        with open('config.xml', 'w') as fd:
            xml['CONFIG']['AUTOMATIC_TIME_SCHEDULE'] = schedule
            fd.write(xmltodict.unparse(xml))

        self.dismiss()
