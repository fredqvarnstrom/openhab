# -*- coding: iso8859-15 -*-

"""
    Utility for date & time operations
"""
import os
import subprocess
import time

import logger


def is_rpi():
    return os.path.exists('/opt/vc/include/bcm_host.h')


def update_system_datetime(utc_dt):
    str_new_time = utc_dt.strftime('%Y-%m-%d %H:%M:%S')
    if is_rpi():
        p = subprocess.Popen(["date --set='{}'".format(str_new_time)], shell=True)
        p.communicate()
        p.wait()
        logger.debug('Updating system date & time: {}'.format(str_new_time))
    else:
        logger.debug('Setting new UTC date & time: {}'.format(str_new_time))


def format_time(_time):
    elapsed = int(time.time()) - _time
    if elapsed < 10:
        return 'Just Now'
    elif elapsed < 60:
        return '{} seconds ago'.format(elapsed)
    elif elapsed < 120:
        return 'A minute ago'.format(elapsed)
    elif elapsed < 3600:
        return '{} minutes ago'.format(int(elapsed / 60))
    elif elapsed < 7200:
        return 'An hour ago'
    elif elapsed < 3600 * 24:
        return '{} hours ago'.format(int(elapsed / 3600))
    elif elapsed < 3600 * 24 * 2:
        return 'A day ago'
    elif elapsed < 3600 * 24 * 30:
        return '{} days ago'.format(int(elapsed / 3600 / 24))
    elif elapsed < 3600 * 24 * 30 * 2:
        return 'A month ago'
    elif elapsed < 3600 * 24 * 30 * 12:
        return '{} months ago'.format(int(elapsed / 3600 / 24 / 30))
    elif elapsed < 3600 * 24 * 30 * 12 * 2:
        return 'A year ago'
    else:
        return '{} years ago'.format(int(elapsed / 3600 / 24 / 30 / 12))
