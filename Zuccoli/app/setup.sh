#!/usr/bin/env bash

echo "========== Setup Wireless Scale Display =========="
sudo apt-get update

# Setup WiFi AP
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
sudo bash rpi3_ap_setup.sh

echo "Install Kivy now..."
sudo apt-get install -y python libpython-dev python-pip
sudo apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev
sudo apt-get install -y libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-bad
sudo apt-get install -y gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-omx
sudo apt-get install -y gstreamer1.0-alsa python-dev libmtdev-dev xclip

sudo pip install -U pip
sudo pip install Cython==0.25.2

sudo pip install git+https://github.com/kivy/kivy.git@master

sudo apt-get install -y python-smbus i2c-tools

# Install mosquitto MQTT broker.
sudo pip install paho-mqtt
sudo apt-get install -y mosquitto mosquitto-clients

sudo pip install pymongo xlsxwriter

# Enable SSH
sudo touch /boot/ssh

# Increase GPU memory size
echo "gpu_mem=512" | sudo tee -a /boot/config.txt
echo "lcd_rotate=2" | sudo tee -a /boot/config.txt

echo "Disabling the booting logo..."
echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt

# Disable the blinking cursor
sudo sed -i -- "s/^exit 0/TERM=linux setterm -foreground black >\/dev\/tty0\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/TERM=linux setterm -clear all >\/dev\/tty0\\nexit 0/g" /etc/rc.local

# Disable some services to reduce booting time
sudo systemctl disable hciuart
sudo mkdir /etc/systemd/system/networking.service.d
sudo touch /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "[Service]" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "TimeoutStartSec=1" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
sudo rm /etc/systemd/system/dhcpcd.service.d/wait.conf
