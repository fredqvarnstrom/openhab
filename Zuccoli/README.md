# Zuccoli Project
======================================================================================

## Synchronize with RTC module (PCF8523)

  https://learn.adafruit.com/adding-a-real-time-clock-to-raspberry-pi/overview

## Install the latest JAVA on RPi 3

    sudo nano /etc/apt/sources.list.d/webupd8team-java.list

And add this:

    deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main
    deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main

After saving this file, execute below:

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886

    sudo apt-get update
    sudo apt-get -y install oracle-java8-installer
    sudo apt-get -y install oracle-java8-set-default


## Install openHAB on RPi3
    
- Install openHAB1.8.3 as indicated in the document of root.

        sudo mkdir /opt/openhab
		cd /opt/openhab
        sudo wget https://bintray.com/artifact/download/openhab/bin/distribution-1.8.3-runtime.zip
        sudo wget https://bintray.com/artifact/download/openhab/bin/distribution-1.8.3-addons.zip
		sudo unzip distribution-1.8.3-runtime.zip
		sudo unzip distribution-1.8.3-addons.zip -d addons

		sudo cp configurations/openhab_default.cfg configurations/openhab.cfg

- Add `openHAB Cloud Connector` addon

        cd /opt/openhab/addons
        sudo wget https://dl.bintray.com/openhab/bin/org.openhab.io.openhabcloud_1.9.0.201612192331.jar

- After starting openhab server with `sudo ./start.sh`, please stop server and retrieve `UUID` and `Secret` value.

    Those values are at `/opt/openhab/webapps/static/uuid` & `/opt/openhab/webapps/static/secret`

    Visit myopenhab.org and sign up with these values.

- Copy our custom configuration files:

        cd ~
        git clone https://bitbucket.org/rpi_guru/openhab.git
        cd openhab/Zuccoli
        sudo cp -R openhab/configurations /opt/openhab


- Enable autostart of openHAB.

    Create configuration file.

        sudo nano /etc/default/openhab.conf

    Add below and and save.

        # PATH TO OPENHAB
        OPENHABPATH=/opt/openhab

        # set ports for HTTP(S) server
        HTTP_PORT=8080
        HTTPS_PORT=8443

    Copy the [openhab auto start script](openhab/autostart/openhab) to `/etc/init.d/`

    Next, I need to set the proper definition. Change current directory to the init.d folder:

        cd /etc/init.d

    Change the file attributes so it can be executed:

        sudo chmod a+x openhab

    Change the group and owner:

        sudo chgrp root openhab
        sudo chown root openhab

    Change to the directory where we have copied openhab.cfg:

        cd /etc/default

    Insert the script into the run level with:

        sudo update-rc.d openhab defaults

    This now starts openHAB at boot time. openHAB can be stopped anytime with

        sudo /etc/init.d/openhab start

    Stopping openHAB works with

        sudo /etc/init.d/openhab stop

    To reboot, use

        sudo reboot

    If I ever want to undo this, openhab can be removed again from the **autostart** with

        sudo update-rc.d -f openhab remove

    I can check if openHAB is running checking if `openhab.pid` is present in `/var/run` with

        ls /var/run

    ![openHAB running](../img/openhab-running.png "openHAB running")


## Install mosquitto on RPi3
    
    sudo pip install paho-mqtt
    cd ~
    mkdir tmp
    cd tmp
    wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
    sudo apt-key add mosquitto-repo.gpg.key
    cd /etc/apt/sources.list.d/
    sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
    sudo apt-get update
    sudo apt-get install mosquitto mosquitto-clients
    sudo chown -R mosquitto:mosquitto /var/log/mosquitto
    sudo service mosquitto restart
    
## Backlight adjustment
    
    sudo nano /etc/udev/rules.d/backlight-permissions.rules

Insert the line:

    SUBSYSTEM=="backlight",RUN+="/bin/chmod 666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power"

## Disable Screen Saver

Add follow in `/etc/rc.local` before `exit 0`:

    sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"

## Increase GPU memory

    sudo raspi-config

Go to `Advanced Option -> Memory Split` and increase GPU memory to **384MB**


## Launch GUI app
    
    sudo pip install xmltodict wifi
    cd app
    sudo python main.py

## Visit https://myopenhab.org/openhab.app and type username & password of your account(myopenhab.org)


## Screen Calibration for the 5" touchscreen from WaveShare
    
Open `~/.kivy/config.ini` and modify like this:
    
    [input]
    mouse = mouse
    #%(name)s = probesysfs,provider=hidinput
    mtdev_%(name)s = probesysfs,provider=mtdev,param=invert_x=1,param=rotation=180
    hid_%(name)s = probesysfs,provider=hidinput,param=invert_x=1,param=rotation=180

## How to find the UUID & Secret for OpenHab Cloud?
    
    cat /opt/openhab/webapps/static/uuid
    cat /opt/openhab/webapps/static/secret 


## Configure UPS PIco HV3.0A/B/B+ HAT

Check ![this](28_0x40_W_UPS%20PIco%20HV3.0.pdf) file.
