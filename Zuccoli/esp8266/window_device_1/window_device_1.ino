/*
	Zuccoli  Window1

	This device will have peripherals below:
	  a window,
	  temperature sensor(In & Out),
	  wind sensor,
	  rain sensor,
	  FIRE switch,
	  AIR switch,
	  AFTER_HOURS button
*/

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>
#include "Adafruit_MCP23008.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>

const char* device_name = "window1";   // Change this value for each WiFi devices

int upload_interval = 30;       // Uploading interval of DS18B20's data in seconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

const char* temp_suffix = "/temperature";

const char* rain_state_suffix = "/rain";

const char* wind_speed_suffix = "/wind_speed";
const char* wind_dir_suffix = "/wind_dir";

const char* w_open_set_suffix = "/w_open/set";
const char* w_open_state_suffix = "/w_open/state";

const char* w_close_set_suffix = "/w_close/set";
const char* w_close_state_suffix = "/w_close/state";

const char* air_state_suffix = "/air/state";
const char* fire_state_suffix = "/fire/state";
const char* after_hours_state_suffix = "/after_hours/state";
const char* security_state_suffix = "/security_close/state";

#define ONE_WIRE_BUS          0       		// D3, GPIO0
#define PIN_WIND              2           // Use D4(GPIO2) as Wind Speed pin
#define PIN_DHT               12					// Use D6, GPIO12
#define PIN_AFTER_HOURS       13    		  // After Hours Switch - D7/GPIO13

Adafruit_MCP23008 mcp23008;
// MCP23008 channels
#define CH_W_OPEN           0         			  // CH0 of MCP23008
#define CH_W_CLOSE          3        			    // CH3 of MCP23008
#define CH_RAIN             7           			// Use CH7 of MCP3008
#define CH_FIRE             6           			// CH3 of MCP3008
#define CH_AIR              2            			// Air Conditioning channel - CH2
#define CH_SECURITY         4            			// Security Close channel - CH4

/* Assign a unique ID to this sensor at the same time */
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);
sensors_event_t accel_event;

float temp_c;

WiFiClient espClient;
PubSubClient client(espClient);

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);

long lastMsg = 0;

const char* w_open_state = "OFF";
const char* w_close_state = "OFF";

float wind_dir = 0;       						// Wind direction
float wind_speed = 0.0;     					// Wind speed

char buf_pub_topic[50];
char buf_sub_topic[50];

int rotations = 0; 										// cup rotation counter used in interrupt routine
volatile unsigned long ContactBounceTime = 0; 				// Timer to avoid contact bounce in interrupt routine

int counter = 0;
int counter_10ms = 0;

float acc_temp = 0;
int counter_temp = 0;

bool after_hours_state = false;
bool last_after_hours_state = false;
unsigned long last_after_hours_time = 0;


String macAddr;

void setup() {

  Serial.begin(9600);

  pinMode(A0, INPUT);           			// Initialize the RAIN DIR SENSOR pin as an input
  pinMode(PIN_WIND, INPUT);       			// Initialize the WIND SPEED pin as an input
  attachInterrupt(PIN_WIND, rotation, FALLING);
  pinMode(PIN_AFTER_HOURS, INPUT);

  DS18B20.begin();

  mcp23008.begin(7);
  mcp23008.pinMode(CH_W_OPEN, OUTPUT);
  mcp23008.digitalWrite(CH_W_OPEN, LOW);
  mcp23008.pinMode(CH_W_CLOSE, OUTPUT);
  mcp23008.digitalWrite(CH_W_CLOSE, LOW);
  mcp23008.pinMode(CH_AIR, INPUT);
  mcp23008.pullUp(CH_AIR, HIGH);  		// turn on a 10K pullup internally
  mcp23008.pinMode(CH_FIRE, INPUT);
  mcp23008.pullUp(CH_FIRE, HIGH);
  mcp23008.pinMode(CH_RAIN, INPUT);
  mcp23008.pinMode(CH_SECURITY, INPUT);

  accel.begin();
  accel.setRange(ADXL345_RANGE_16_G);
  // displaySetRange(ADXL345_RANGE_8_G);
  // displaySetRange(ADXL345_RANGE_4_G);
  // displaySetRange(ADXL345_RANGE_2_G);

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  counter = 0;
  acc_temp = 0;
  rotations = 0;
}

void setup_wifi() {

  int attempt = 0;

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println(); Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
	// Try to connect for 15 sec, and restart
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    Serial.print(".");
	delay(500);
  }
  Serial.println(""); Serial.println("WiFi connected"); Serial.println("IP address: "); Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  // Check topic of windows open relay
  set_sub_topic(w_open_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    if (!strncmp((const char*)payload, "ON", 2)) {
      if (!strncmp(w_close_state, "ON", 2)) {
        Serial.println("Cannot turn WINDOW OPEN relay on because CLOSE is already ON!");
      }
      else {
        w_open_state = "ON";
        mcp23008.digitalWrite(CH_W_OPEN, HIGH);
      }
    } else if (!strncmp((const char*)payload, "OFF", 3)){
      w_open_state = "OFF";
      mcp23008.digitalWrite(CH_W_OPEN, LOW);
    }
  }

  // Check topic of windows close relay
  else{
    set_sub_topic(w_close_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
      if (!strncmp((const char*)payload, "ON", 2)) {
        if (!strncmp(w_open_state, "ON", 2)) {
          Serial.println("Cannot turn WINDOW CLOSE relay on because OPEN is already ON!");
        }
        else {
          w_close_state = "ON";
          mcp23008.digitalWrite(CH_W_CLOSE, HIGH);
        }
      } else if (!strncmp((const char*)payload, "OFF", 3)){
        w_close_state = "OFF";
        mcp23008.digitalWrite(CH_W_CLOSE, LOW);
      }
    }
  }
}

void reconnect() {

  int attempt = 0;

  // Loop until we're reconnected
  while (!client.connected()) {
    if (attempt < 3)
      attempt ++;
    else
      ESP.restart();
    // Attempt to connect
    if (client.connect(macAddr.c_str())) {
	  Serial.println("Connected to the MQTT server... ");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(w_open_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(w_close_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    } else {
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  bool reading = digitalRead(PIN_AFTER_HOURS);
  if (reading != last_after_hours_state){
    last_after_hours_time = millis();
  }
  if ((millis() - last_after_hours_time) > 200)
    if (reading != after_hours_state){
      after_hours_state = reading;
      if (reading == HIGH){
        set_pub_topic(after_hours_state_suffix);
  	    client.publish(buf_pub_topic, "ON");
	    Serial.println("==== After Hours switch is triggered!");
      }
    }
  last_after_hours_state = reading;

  if (counter_10ms < 99){
	counter_10ms ++;
	delay(10);
	return;
  }
  else
	counter_10ms = 0;

  accel.getEvent(&accel_event);
  /* Display the results (acceleration is measured in m/s^2) */
  Serial.print("X: "); Serial.print(accel_event.acceleration.x); Serial.print("  ");
  Serial.print("Y: "); Serial.print(accel_event.acceleration.y); Serial.print("  ");
  Serial.print("Z: "); Serial.print(accel_event.acceleration.z); Serial.print("  ");Serial.println("m/s^2 ");


  int attempt_temp = 0;
  do {
    DS18B20.requestTemperatures();
    temp_c = DS18B20.getTempCByIndex(0);
    // Try to read for 5 times
    if (attempt_temp < 5)
        attempt_temp ++;
    else
        break;
  } while (temp_c == 85.0 || temp_c == (-127.0));

  Serial.print("Temperature from DS18B20: ");
  Serial.print(temp_c);

  // We accumulate the rotation events for 60 seconds, and publish wind speed after calculating it.
  if (counter < upload_interval - 1){
	// Accumulate temperature
	if (temp_c != 85.0 && temp_c != (-127.0)){
	  acc_temp += temp_c;
	  counter_temp ++;
	}

    // publish state of rain sensor
    set_pub_topic(rain_state_suffix);
	if (mcp23008.digitalRead(CH_RAIN)) client.publish(buf_pub_topic, "ON");
	else client.publish(buf_pub_topic, "OFF");
    Serial.print("  RAIN: ");	Serial.print(mcp23008.digitalRead(CH_RAIN));

    // publish wind direction
    read_wind();
    char* buf_dir = new char[10];
    dtostrf(wind_dir, 5, 2, buf_dir);
    set_pub_topic(wind_dir_suffix);
    client.publish(buf_pub_topic, buf_dir);
	Serial.print("  WIND DIR: ");	Serial.print(wind_dir);

    // publish fire
    set_pub_topic(fire_state_suffix);
	if (mcp23008.digitalRead(CH_FIRE)) client.publish(buf_pub_topic, "ON");
	else client.publish(buf_pub_topic, "OFF");
	Serial.print("  FIRE: ");	Serial.print(mcp23008.digitalRead(CH_FIRE));

    // publish air
    set_pub_topic(air_state_suffix);
    if (mcp23008.digitalRead(CH_AIR)) client.publish(buf_pub_topic, "OFF");
	else client.publish(buf_pub_topic, "ON");
    Serial.print("  AIR: "); Serial.print(!mcp23008.digitalRead(CH_AIR));

    // publish Security Close Pin
    set_pub_topic(security_state_suffix);
    if (mcp23008.digitalRead(CH_SECURITY)) client.publish(buf_pub_topic, "ON");
	else client.publish(buf_pub_topic, "OFF");
    Serial.print("  SECURITY_CLOSE: "); Serial.println(mcp23008.digitalRead(CH_SECURITY));

    // publish state of window open relay
    set_pub_topic(w_open_state_suffix);
    client.publish(buf_pub_topic, w_open_state);
    // publish state of window close relay
    set_pub_topic(w_close_state_suffix);
    client.publish(buf_pub_topic, w_close_state);

	Serial.print("WINDOW: OPEN => "); Serial.print(w_open_state); Serial.print(" , CLOSE => "); Serial.println(w_close_state);

	counter ++;
  }
  else{
    // If temperature is valid
    if (temp_c != 85.0 && temp_c != (-127.0)){
	  acc_temp += temp_c;
	  counter_temp ++;
    }

    if (counter_temp > 0){
      char* buf_temp = new char[10];
	  dtostrf(acc_temp / float(counter_temp), 5, 2, buf_temp);

	  // publish temperature value
	  set_pub_topic(temp_suffix);
	  client.publish(buf_pub_topic, buf_temp);

	  Serial.print("==== Temperature: "); Serial.print(buf_temp);

	  acc_temp = 0;
	  counter_temp = 0;
    }

	// convert to mp/h using the formula V=P(2.25/T)
    // V = P(2.25/60)
    wind_speed = rotations * 2.25 / 60.0;

    // publish wind speed
    char* buf_speed = new char[10];
    dtostrf(wind_speed, 5, 2, buf_speed);
    set_pub_topic(wind_speed_suffix);
    client.publish(buf_pub_topic, buf_speed);
	Serial.print("===== WIND SPEED: "); Serial.print(buf_speed);

    rotations = 0;
    counter = 0;
  }

}

void read_wind(){
  // Read wind speed & direction
  int a_val = analogRead(A0);
  wind_dir = (float)a_val / 1024.0 * 360.0;  // We used 5-times divider since A0 has range of 0~1V
}

void rotation(){
  if ((millis() - ContactBounceTime) > 15 ) { // debounce the switch contact.
  rotations++;
  ContactBounceTime = millis();
  }
}

void set_pub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}
