import argparse
import os
import sys
import fileinput


def add_device(dev_name):
    """
    Added device
    """
    if not add_item(dev_name):
        return False

    if not add_persistence(dev_name):
        return False

    if not add_rule(dev_name):
        return False

    if not add_sitemap(dev_name):
        return False

    print "Added device successfully."
    return True


def add_item(dev):
    """
    Add items with given device name
    """
    f = open("configurations/items/default.items", 'r+b')
    f_content = f.readlines()

    if ('"' + dev + '"') in ''.join(f_content):
        print "This device is already exists."
        return False

    f_content.append('\n')
    f_content.append('Group g' + dev + ' "' + dev + '"(All)\n')
    f_content.append('Number ' + dev + '_T "TEMPERATURE [%.2f C]" <temperature> (g' + dev +
                     ') {mqtt="<[mosquitto:' + dev + '/temperature:state:default]"}\n')
    f_content.append('Number ' + dev + '_H "HUMIDITY [%.2f %%]" <humidity> (g' + dev +
                     ') {mqtt="<[mosquitto:' + dev + '/humidity:state:default]"}\n')
    f_content.append('String ' + dev + '_R "RAIN [%s]" <rain> (g' + dev + ') {mqtt="<[mosquitto:' +
                     dev + '/rain:state:default]"}\n')
    f_content.append('Switch ' + dev + '_W_OPEN "WINDOW OPEN" (g' + dev + ') {mqtt=">[mosquitto:' + dev +
                     '/w_open/set:command:on:ON],>[mosquitto:' + dev + '/w_open/set:command:off:OFF],<[mosquitto:' +
                     dev + '/w_open/state:state:default"}\n')
    f_content.append('Switch ' + dev + '_W_CLOSE "WINDOW CLOSE" (g' + dev + ') {mqtt=">[mosquitto:' + dev +
                     '/w_close/set:command:on:ON],>[mosquitto:' + dev + '/w_close/set:command:off:OFF],<[mosquitto:' +
                     dev + '/w_close/state:state:default"}\n')
    f_content.append('Number ' + dev + '_CHART "CHART" (g' + dev + ')\n')
    # return pointer to top of file so we can re-write the content with replaced string
    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


def add_persistence(dev):
    """
    Add persistence rule
    """
    f = open("configurations/persistence/rrd4j.persist", 'r+b')
    f_content = f.readlines()

    if (dev + '_T') in ''.join(f_content):
        print "This device is already exists."
        return False
    f_content.append('\n')
    f_content.append('Items {\n')
    f_content.append('    ' + dev + '_T, ' + dev + '_H : strategy = everyUpdate\n')
    f_content.append('}\n')

    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


def add_rule(dev):
    """
    Add rule
    """
    # TODO: need to tweak!!!
    f = open("configurations/rules/default.rules", 'r+b')
    f_content = f.readlines()

    if (dev + '_rain_on') in ''.join(f_content):
        print "This device is already exists."
        return False

    f_content.append('\nrule "' + dev + '_rain_on"\n')
    f_content.append('    when\n        Item ' + dev + '_R changed from OFF to ON\n')
    f_content.append('    then\n        sendCommand(' + dev + '_W_OPEN, OFF)\n        sendCommand(' + dev +
                     '_W_CLOSE, ON)\n    end\n')
    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


def add_sitemap(dev):
    """
    Add sitemap
    """
    f = open("configurations/sitemaps/default.sitemap", 'r+b')
    f_content = f.readlines()

    if ('label="' + dev + '"') in ''.join(f_content):
        print "This device is already exists."
        return False

    # find the final "}" to add frames before it.
    f_line = 1
    for i in range(1, len(f_content)):
        if f_content[-i].strip() == "}":
            f_line = i
            break

    f_content.insert(-f_line, '\n')
    f_content.insert(-f_line, '    Frame label="' + dev + '"{\n        Text item=' + dev + '_T\n        Text item=' +
                     dev + '_H\n        Text label="Temperature/Humidity" icon="chart"{\n            Frame{\n        '
                     '        Switch item=' + dev + '_CHART label="Chart Period" mappings=[0="Hour", 1="Day", 2="Week"]'
                                                    '\n                Chart item=g' + dev +
                     ' period=h refresh=10000 visibility=[' + dev + '_CHART==0, ' + dev + '_CHART=="Uninitialized"]\n')
    f_content.insert(-f_line, '                Chart item=g' + dev + ' period=D refresh=60000 visibility=[' + dev +
                     '_CHART==1]\n                Chart item=g' + dev + ' period=W refresh=60000 visibility=[' + dev +
                     '_CHART==2]\n            }\n        }\n        Text item=' + dev + '_R\n        Switch item=' +
                     dev + '_W_OPEN\n        Switch item=' + dev + '_W_CLOSE\n    }\n')
    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


def remove_device(dev_name):
    """
    Remove device
    """
    if not remove_item(dev_name):
        return False

    if not remove_persistence(dev_name):
        return False

    if not remove_rule(dev_name):
        return False

    if not remove_sitemap(dev_name):
        return False

    print "Removed device successfully."

    return True


def remove_item(dev):

    line_count = 7      # line count of each device

    f = open("configurations/items/default.items", 'r+b')
    f_content = f.readlines()

    c_str = 'Group g' + dev + ' "' + dev + '"(All)'

    line_num = get_involve_index(f_content, c_str)
    if line_num < 0:
        print "No such device in item list."
        return False

    for i in range(line_count):
        f_content.pop(line_num)
    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


def get_involve_index(dest_list, val):
    """
    Return index in the dest_list which contains val
    """
    result = -1
    for d in dest_list:
        if val in d:
            result = dest_list.index(d)
            break
    return result


def remove_persistence(dev):
    line_count = 3  # line count of each device

    f = open("configurations/persistence/rrd4j.persist", 'r+b')
    f_content = f.readlines()

    c_str = dev + '_T, ' + dev + '_H : strategy = everyUpdate'

    line_num = get_involve_index(f_content, c_str)
    if line_num < 0:
        print "No such device in persistence."
        return False

    for i in range(line_count):
        f_content.pop(line_num - 1)
    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


def remove_rule(dev):
    line_count = 7  # line count of each device

    f = open("configurations/rules/default.rules", 'r+b')
    f_content = f.readlines()

    c_str = 'rule "' + dev + '_rain_on"'

    line_num = get_involve_index(f_content, c_str)
    if line_num < 0:
        print "No such device in rules."
        return False

    for i in range(line_count):
        f_content.pop(line_num)
    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


def remove_sitemap(dev):
    line_count = 15  # line count of each device

    f = open("configurations/sitemaps/default.sitemap", 'r+b')
    f_content = f.readlines()

    c_str = 'Frame label="' + dev + '"{'

    line_num = get_involve_index(f_content, c_str)
    if line_num < 0:
        print "No such device in sitemap."
        return False

    for i in range(line_count):
        f_content.pop(line_num)
    f.seek(0)
    # clear file content
    f.truncate()
    # re-write the content with the updated content
    f.write(''.join(f_content))
    # close file
    f.close()
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--action_type", required=True, help="Action type, should be 'add' or 'remove'")
    parser.add_argument("-d", "--device_name", required=True, help="Device name. (Case sensitive)")

    inst_arg = parser.parse_args()
    args = vars(inst_arg)

    action_type = args['action_type']
    device_name = args['device_name']

    if action_type == 'add':
        add_device(device_name)
    elif action_type == 'remove':
        remove_device(device_name)
    else:
        print "Error, action type should be 'add' or 'remove'"
        exit(0)

    # add_device('TMP')
    # remove_device('TMP')
