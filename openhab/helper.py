import datetime
import paho.mqtt.client as mqtt
import time

device_list = []
device_status = {}


# The callback for when the client receives a CONNECT response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    for dev in device_list:
        client.subscribe(dev + "/w_open/set")
        client.subscribe(dev + "/w_open/state")
        client.subscribe(dev + "/w_close/set")
        client.subscribe(dev + "/w_close/state")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    """
    If command is 'set' and value is 'ON', store this moment to the variable, and starts monitoring its state.
    Monitoring: get payload of its status topic and set ON again if its value is OFF
    :return:
    """
    # print(msg.topic + " -> " + str(msg.payload))
    # Sample topic : Office/w_open/set, Office/w_close/state
    topic = msg.topic
    topic = topic.split('/')
    dev = topic[0]

    dev_key = dev + "_" + topic[1]
    if topic[2] == 'set':
        if msg.payload == 'ON':
            if time.time() - device_status[dev_key] > 90:
                device_status[dev_key] = time.time()
                print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), ": Started to run, ", topic
                device_status[dev_key + "_remaining"] = 90
                device_status[dev_key + "_restore_time"] = time.time()

        elif msg.payload == 'OFF':
            device_status[dev_key] = 0
            device_status[dev_key + "_restore_time"] = 0
            device_status[dev_key + "_remaining"] = 0
        return
    elif topic[2] == 'state':
        if msg.payload == 'OFF' and time.time() - device_status[dev_key] < 90:
            client.publish(dev + "/" + topic[1] + "/set", 'ON')
            # Calculate remaining time and store
            device_status[dev_key + "_restore_time"] = time.time()
            # It take about 5 sec to reboot ESP...
            device_status[dev_key + "_remaining"] = device_status[dev_key] - time.time() + 90 + 5
            print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), ":  Set back, ", topic
            return True

        elif msg.payload == 'ON':
            try:
                # Get remaining time and turn it off when time is over
                r_time = device_status[dev_key + "_restore_time"]
                t_remain = device_status[dev_key + "_remaining"]
                if time.time() - r_time > t_remain:
                    client.publish(dev + "/" + topic[1] + "/set", 'OFF')

            except KeyError:
                pass

    # print device_status, "  ", time.time()


def find_registered_devices():
    """
    Find registered devices
    :return: List of device names
    """
    try:
        f = open("configurations/sitemaps/default.sitemap", 'r')
    except IOError:
        f = open("/opt/openhab/configurations/sitemaps/default.sitemap", 'r')

    d_list = []
    f_content = f.readlines()
    for cont in f_content:
        if "Frame label=" in cont:
            dev_name = cont.split('"')[1]
            d_list.append(dev_name)

    return d_list


if __name__ == '__main__':

    global device_list
    global device_status

    device_list = find_registered_devices()

    for dev in device_list:
        device_status[dev + '_w_open'] = 0  # initialize status of this device to 0
        device_status[dev + '_w_close'] = 0  # initialize status of this device to 0

    print "Discovered devices: "
    print device_list

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    # client.connect("192.168.1.110", 1883, 60)
    client.connect("127.0.0.1", 1883, 60)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    client.loop_forever()


