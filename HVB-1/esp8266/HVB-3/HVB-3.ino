/*
  
  HVB-1  Window3
  
  This device will have a windows, rain sensor, and wind station.
  
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* device_name = "window3";   // Change this value for each WiFi devices

int upload_interval = 1000;       // Uploading interval of DHT22 data in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
//const char* ssid = "RPi3-AP";              // RPi3's AP name
//const char* password = "raspberry";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

const char* rain_state_suffix = "/rain";

const char* wind_speed_suffix = "/wind_speed";
const char* wind_dir_suffix = "/wind_dir";

const char* w_open_set_suffix = "/w_open/set";
const char* w_open_state_suffix = "/w_open/state";

const char* w_close_set_suffix = "/w_close/set";
const char* w_close_state_suffix = "/w_close/state";

int PIN_W_OPEN = 5;         // Use D1, GPIO5
int PIN_W_CLOSE = 4;        // Use D2, GPIO4
int PIN_WIND = 12;        // D6, Wind Speed pin, 
int PIN_RAIN = 14;        // Use D5, GPIO14

int LED_PIN = 16;       // Built-in LED pin

float wind_dir = 0;       // Wind direction
float wind_speed = 0.0;     // Wind speed

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;

const char* w_open_state = "OFF";
const char* w_close_state = "OFF";

char buf_pub_topic[50];
char buf_sub_topic[50];

int rotations = 0; // cup rotation counter used in interrupt routine 
volatile unsigned long ContactBounceTime; // Timer to avoid contact bounce in interrupt routine
int counter = 0;

void setup() {
  pinMode(PIN_W_OPEN, OUTPUT);      // Initialize the BUILTIN_LED pin as an output
  digitalWrite(PIN_W_OPEN, LOW);
  pinMode(PIN_W_CLOSE, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite(PIN_W_CLOSE, LOW);
  
  pinMode(PIN_RAIN, INPUT);       // Initialize the RAIN SENSOR pin as an input
  
  pinMode(A0, INPUT);           // Initialize the RAIN SENSOR pin as an input
  pinMode(PIN_WIND, INPUT);       // Initialize the WIND SPEED pin as an input
  
  pinMode(LED_PIN, LOW);
  digitalWrite(LED_PIN, LOW);

  attachInterrupt(PIN_WIND, rotation, FALLING); 
  
  Serial.begin(9600);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  counter = 0;
  rotations = 0;
}

void setup_wifi() {
	
  int attempt = 0;
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
	// Try to connect for 15 sec, and restart
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    digitalWrite(LED_PIN, LOW);
    delay(250);
    digitalWrite(LED_PIN, HIGH);
    delay(250);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");
  
  // Check topic of windows open relay
  set_sub_topic(w_open_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    if (!strncmp((const char*)payload, "ON", 2)) {
        digitalWrite(PIN_W_OPEN, HIGH);     
    w_open_state = "ON";
        Serial.println("Turning RELAY of WINDOWS OPEN ON...");
        
    } else if (!strncmp((const char*)payload, "OFF", 3)){
      digitalWrite(PIN_W_OPEN, LOW);  
    w_open_state = "OFF";
      Serial.println("Turning RELAY of WINDOWS OPEN OFF...");
      
    }
  }   
  
  // Check topic of windows close relay
  else{
    set_sub_topic(w_close_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
      if (!strncmp((const char*)payload, "ON", 2)) {
            digitalWrite(PIN_W_CLOSE, HIGH);   
      w_close_state = "ON";
            Serial.println("Turning RELAY of WINDOWS CLOSE ON...");
      } else if (!strncmp((const char*)payload, "OFF", 3)){
        digitalWrite(PIN_W_CLOSE, LOW);  
    w_close_state = "OFF";
        Serial.println("Turning RELAY of WINDOWS CLOSE OFF...");
        
      }
    }
  }
}

void reconnect() {
  int attempt = 0;
  // Loop until we're reconnected
  while (!client.connected()) {
    if (attempt < 3)
      attempt ++;
    else
      ESP.restart();
	  
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client3")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(w_open_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(w_close_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  // We accumulate the rotation events for 60 seconds, and publish wind speed after calculating it.
  if (counter < 59){
  // publish state of rain sensor
    set_pub_topic(rain_state_suffix);
    if (digitalRead(PIN_RAIN) == HIGH)
      client.publish(buf_pub_topic, "OFF");
    else{
      Serial.println("Warning, rain is detected!");
      client.publish(buf_pub_topic, "ON");
    }
  
    // publish wind direction
    read_wind();
    char* buf_dir = new char[10];
    dtostrf(wind_dir, 5, 2, buf_dir);
    set_pub_topic(wind_dir_suffix);
    client.publish(buf_pub_topic, buf_dir);
    Serial.print("Wind direction: ");
    Serial.println(wind_dir);
    
    // publish state of window open relay 
    set_pub_topic(w_open_state_suffix);
    client.publish(buf_pub_topic, w_open_state);

    // publish state of window close relay
    set_pub_topic(w_close_state_suffix);
    client.publish(buf_pub_topic, w_close_state);
  counter ++; 
  }
  else{
  // convert to mp/h using the formula V=P(2.25/T) 
    // V = P(2.25/60)

    wind_speed = rotations * 2.25 / 60.0;
  
    // publish wind speed
    char* buf_speed = new char[10];
    dtostrf(wind_speed, 5, 2, buf_speed);
    set_pub_topic(wind_speed_suffix);
    client.publish(buf_pub_topic, buf_speed);
    Serial.print("Wind Speed: ");
    Serial.println(wind_speed);
  rotations = 0;
  counter = 0;
  }
  
  delay(1000);
  
}


void read_wind(){
  // Read wind speed & direction
  int a_val = analogRead(A0);
  wind_dir = (float)a_val / 1024.0 * 360.0;  // We used 5-times divider since A0 has range of 0~1V
  
}

void rotation(){
  if ((millis() - ContactBounceTime) > 15 ) { // debounce the switch contact. 
  rotations++; 
  ContactBounceTime = millis(); 
  } 
}

void set_pub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}



