# Working with Raspberry Pi.

## Setup Raspberry Pi 3 as WiFi Access Point

- Install packages.

        sudo apt-get install dnsmasq hostapd

- CONFIGURE YOUR INTERFACES

    The first thing you'll need to do is to configure your `wlan0` interface with a static IP.

    In newer Raspian versions, interface configuration is handled by `dhcpcd` by default. 
    We need to tell it to ignore `wlan0`, as we will be configuring it with a static IP address elsewhere. 

    So open up the `dhcpcd` configuration file with `sudo nano /etc/dhcpcd.conf` and add the following line to the bottom of the file:

        denyinterfaces wlan0  
    
    Note: This must be ABOVE any interface lines you may have added!

    Now we need to configure our static IP. 

    To do this open up the interface configuration file with `sudo nano /etc/network/interfaces` and edit the `wlan0` section so that it looks like this:

        allow-hotplug wlan0  
        iface wlan0 inet static  
            address 172.24.1.1
            netmask 255.255.255.0
            network 172.24.1.0
            broadcast 172.24.1.255
        #    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
    
    Restart `dhcpcd` with `sudo service dhcpcd restart` and then reload the configuration for `wlan0` with `sudo ifdown wlan0; sudo ifup wlan0`.

- CONFIGURE HOSTAPD
    
    Next, we need to configure `hostapd`. 
    
    Create a new configuration file with `sudo nano /etc/hostapd/hostapd.conf` with the following contents:

        # This is the name of the WiFi interface we configured above
        interface=wlan0
        
        # Use the nl80211 driver with the brcmfmac driver
        driver=nl80211
        
        # This is the name of the network
        ssid=WiFi560-AP
        
        # Use the 2.4GHz band
        hw_mode=g
        
        # Use channel 1
        channel=1
        
        # Enable 802.11n
        ieee80211n=1
        
        # Enable WMM
        wmm_enabled=1
        
        # Enable 40MHz channels with 20ns guard interval
        # ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
        
        # Accept all MAC addresses
        macaddr_acl=0
        
        # Use WPA authentication
        auth_algs=1
        
        # Require clients to know the network name
        ignore_broadcast_ssid=0
        
        # Use WPA2
        wpa=2
        
        # Use a pre-shared key
        wpa_key_mgmt=WPA-PSK
        
        # The network passphrase
        wpa_passphrase=Shane_wifi560
        
        # Use AES, instead of TKIP
        rsn_pairwise=CCMP
    
    NOTE: We assigned **SSID & PASSWORD** as `WiFi560-AP & Shane_wifi560` and these values will be embedded into the firmware of ESP.
        
    We can check if it's working at this stage by running `sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf`. 
    
    If it's all gone well thus far, you should be able to see to the network **Pi3-AP**! 
    
    If you try connecting to it, you will see some output from the Pi, but you won't receive and IP address until we set up `dnsmasq` in the next step. 
    
    Use **Ctrl+C** to stop it.

    We aren't quite done yet, because we also need to tell `hostapd` where to look for the config file when it starts up on boot. 
    
    Open up the default configuration file with `sudo nano /etc/default/hostapd` and find the line `#DAEMON_CONF=""` and replace it with `DAEMON_CONF="/etc/hostapd/hostapd.conf"`.
    
- CONFIGURE DNSMASQ
    
    The shipped `dnsmasq` config file contains a wealth of information on how to use it, but the majority of it is largely redundant for our purposes. 
    
    I'd advise moving it (rather than deleting it), and creating a new one with

        sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig  
        sudo nano /etc/dnsmasq.conf  
    
    Paste the following into the new file:

        interface=wlan0                 # Use interface wlan0  
        listen-address=172.24.1.1       # Explicitly specify the address to listen on  
        bind-interfaces                 # Bind to the interface to make sure we aren't sending things elsewhere  
        server=8.8.8.8                  # Forward DNS requests to Google DNS  
        domain-needed                   # Don't forward short names  
        bogus-priv                      # Never forward addresses in the non-routed address spaces.  
        dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time  
        
    You can check with your mobile by trying to access to the RPi3's AP. (WiFi560-AP & Shane_wifi560)
    
## Install openHAB on the Raspberry Pi.

- Expand file system in `raspi-config`.
- Update and upgrade packages.

		sudo apt-get upgrade
		sudo apt-get update

- Install java.

		sudo apt-get install oracle-java7-jdk
		sudo update-java-alternatives -s jdk-7-oracle-armhf

- Install openHAB

		sudo mkdir /opt/openhab
		cd /opt/openhab

        sudo wget https://bintray.com/artifact/download/openhab/bin/distribution-1.8.3-runtime.zip
        sudo wget https://bintray.com/artifact/download/openhab/bin/distribution-1.8.3-addons.zip
		sudo unzip distribution-1.8.3-runtime.zip
		sudo unzip distribution-1.8.3-addons.zip -d addons
		
		sudo cp configurations/openhab_default.cfg configurations/openhab.cfg

	Start openHAB
		
		sudo ./start.sh
		
	It will take few minutes to start.

- Install Habmin
	
		wget https://github.com/cdjackson/HABmin/releases/download/0.1.3-snapshot/habmin.zip
		unzip habmin.zip

	URL: `http://<RPi's IP>:8080/habmin`
	
- Enable autostart of openHAB.

    Create configuration file.
    
        sudo nano /etc/default/openhab.conf
    
    Add below and and save.
        
        # PATH TO OPENHAB
        OPENHABPATH=/opt/openhab
         
        # set ports for HTTP(S) server
        HTTP_PORT=8080
        HTTPS_PORT=8443
    
    Copy the [openhab auto start script](openhab/autostart/openhab) to `/etc/init.d/`
        
    Next, I need to set the proper definition. Change current directory to the init.d folder:

        cd /etc/init.d
    
    Change the file attributes so it can be executed:
        
        sudo chmod a+x openhab
    
    Change the group and owner:
        
        sudo chgrp root openhab
        sudo chown root openhab
    
    Change to the directory where we have copied openhab.cfg:

        cd /etc/default
        
    Insert the script into the run level with:
        
        sudo update-rc.d openhab defaults
        
    This now starts openHAB at boot time. openHAB can be stopped anytime with
        
        sudo /etc/init.d/openhab start
        
    Stopping openHAB works with
        
        sudo /etc/init.d/openhab stop
        
    To reboot, use
        
        sudo reboot
        
    If I ever want to undo this, openhab can be removed again from the **autostart** with
        
        sudo update-rc.d -f openhab remove
        
    I can check if openHAB is running checking if `openhab.pid` is present in `/var/run` with
        
        ls /var/run
    
    ![openHAB running](img/openhab-running.png "openHAB running")
    
    
## Adding authentication to the openHAB server.
    
Open the `configurations/users.cfg`. 
    
    sudo nano /opt/openhab/configurations/users.cfg

Add list of "user=password" pairs like below.

    user=password,user,role
    Intelec=admin61

I added new user with `Intelec/admin61`. 

Open the `configurations/openhab.cfg` and change the Security options.
    
    sudo nano /opt/openhab/configurations/openhab.cfg
    
Original:
    
    #security:option=

Change it to:
    
    security:option=ON

Restart server and it will ask user name and password.
    
## Install mosquitto MQTT broker on RPi.
    
    sudo pip install paho-mqtt
    cd ~
    mkdir tmp
    cd tmp
    wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
    sudo apt-key add mosquitto-repo.gpg.key
    cd /etc/apt/sources.list.d/
    sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
    sudo apt-get update
    sudo apt-get install mosquitto mosquitto-clients
    
    
## Setting up openHAB for MQTT.
 
Open `configurations/openhab.cfg` in your favorite text editor, scroll down to the `**MQTT** transport` section and add this line:

    mqtt:mosquitto.url=tcp://172.24.1.1:1883
    mqtt:mosquitto.user=Intelec
    mqtt:mosquitto.pwd=admin61

Since we had assigned the ip addresses of internal wifi network as 172.24.1.XXX, we set url of mosquitto server to `172.24.1.1`.

So nobody can capture MQTT packets in internal wifi network while he does not login to the internal wifi network.

(To login, he needs to know password of RPi3's AP.)

Create a sitemap file:

    sudo nano /opt/openhab/configurations/sitemaps/default.sitemap
    
Add below:
    
    sitemap default label="Main Menu"
    {
        Frame label="Office"{
            Text item=Office_T
            Text item=Office_H
            Text label="Temperature/Humidity" icon="chart"{
                Frame{
                    Switch item=Office_CHART label="Chart Period" mappings=[0="Hour", 1="Day", 2="Week"]
                    Chart item=gOffice period=h refresh=10000 visibility=[Office_CHART==0, Office_CHART=="Uninitialized"]
                    Chart item=gOffice period=D refresh=60000 visibility=[Office_CHART==1]
                    Chart item=gOffice period=W refresh=60000 visibility=[Office_CHART==2]
                }
            }
            Text item=Office_R
            Switch item=Office_W_OPEN
            Switch item=Office_W_CLOSE
        }
    }

Now the items file:

    sudo nano /opt/openhab/configurations/items/default.items

Add below:

    Group All
    
    Group gOffice "Office" (All)
    Number Office_T "TEMPERATURE [%.2f C]" <temperature> (gOffice) {mqtt="<[mosquitto:Office/temperature:state:default]"}
    Number Office_H "HUMIDITY [%.2f %%]" <humidity> (gOffice) {mqtt="<[mosquitto:Office/humidity:state:default]"}
    String Office_R "RAIN [%s]" <rain> (gOffice) {mqtt="<[mosquitto:Office/rain:state:default]"}
    Switch Office_W_OPEN "WINDOW OPEN" (gOffice) {mqtt=">[mosquitto:Office/w_open/set:command:on:ON],>[mosquitto:Office/w_open/set:command:off:OFF],<[mosquitto:Office/w_open/state:state:default"}
    Switch Office_W_CLOSE "WINDOW CLOSE" (gOffice) {mqtt=">[mosquitto:Office/w_close/set:command:on:ON],>[mosquitto:Office/w_close/set:command:off:OFF],<[mosquitto:Office/w_close/state:state:default"}
    Number Office_CHART "CHART" (gOffice)

Now add persistence.
    
    sudo nano /opt/openhab/configurations/persistence/rrd4j.persist
    
Add below:
    
    Strategies {
        everyMinute : "0 * * * * ?"
        default=everyMinute, everyUpdate, restoreOnStartup
    }
    
    Items {
        Office_T, Office_H : strategy = everyUpdate
    }

Add rules.
    
    sudo nano /opt/openhab/configurations/rules/default.rules

Add below:
    
    rule "Office_rain_on"
        when
            Item Office_R changed from OFF to ON
        then
            sendCommand(Office_W_OPEN, OFF)
            sendCommand(Office_W_CLOSE, ON)
        end


**NOTE:** There must be no space before and after the equal sign.

OpenHAB is now configured to read the two values from the MQTT broker and is subscribed to the `Office/temperature` and `Office/humidity` topics, which have been assigned to the two items.

To update state of 2 relay, it subscribes `Office/w_open/state` and `Office/w_close/state` topic.

And it publishes `ON/OFF` message on `Office/w_open/set`, `Office/w_close/set` topic to turn relays on/off.

The `<humidity>`, `<rain>` icon does not exist but I googled one and uploaded them(**humidity.png**, **rain.png**) to `/opt/openhab/webapps/images/`.
    
## Enabling python helper for ESP
    
    sudo pip install paho-mqtt
    sudo nano /etc/rc.local
    
Added following line before `exit 0`
    
    (sleep 20; /usr/bin/python /opt/openhab/helper.py)&

# Working with ESP8266-12E

## Wiring sensors, relays with ESP.
    
Please refer **Fritzing** [File](Fritzing/wifi560.fzz).

You can download **Fritzing** software from [here](fritzing.org) and add **Adafruit Fritzing Library** from [here](https://github.com/adafruit/Fritzing-Library).

![Wiring Diagram](img/Wiring_diagram.jpg "Wiring Diagram")

For each LED, we can easily replace them with relays at the same pin position. 


## Arduino core for ESP8266 WiFi chip 

Starting with 1.6.4, Arduino allows installation of third-party platform packages using Boards Manager. 

We have packages available for Windows, Mac OS, and Linux (32 and 64 bit).

- Install **Arduino 1.6.5** from the Arduino website.
- Start Arduino and open Preferences window.
- Enter `http://arduino.esp8266.com/stable/package_esp8266com_index.json` into Additional Board Manager URLs field. 
    You can add multiple URLs, separating them with commas.
- Open Boards Manager from `Tools > Board` menu and install esp8266 platform (and don't forget to select your ESP8266 board from Tools > Board menu after installation).

## Install libraries

After you’re done installing, open the **Arduino IDE**.
 
In the menu click on `sketch -> include library -> manage libraries` and install the following libraries:
    
- **pubsubclient** by Nick O'Leary
- **DHT Sensor library** by Adafruit

## Compile the source code and upload to ESP.

Open the Arduino and import [sketch file](ESP8266/MQTT_client/MQTT_client.ino).

Make sure that you have select the correct COM port of ESP.

Upload new sketch by pressing **CTRL+U**.

